<?php
include './Controllers/securityController.php';
include './Controllers/connection.php';
include_once './Controllers/FRASES.php';
validarSesion();
$usuario = $_SESSION['user'];
$idFichaTecnica = filter_input(INPUT_GET, 'id_fichaTecnica');
$query = "SELECT id_tecnico,nombre FROM tecnico WHERE email='$usuario'";
$result_tecnico = mysql_query($query);

$query = "SELECT id_estado,nombre FROM estadoIncidencia ";
$result_estado = mysql_query($query);

$hoy = date("Y-m-d H:i");
?>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>Men&uacute; Ficha T&eacute;cnica</title>
    </head>
    <body>
        <?php include ("./nav.php"); ?>
        <div class="divContenedoraTabla tablaCarga">
            <form name="agregarIncidencia" method="post" action="Controllers/agregarIncidenciaController.php?id_fichaTecnica=<?php echo $idFichaTecnica ?>">
                <table>
                    <tr>
                        <td>
                            <h2><?php echo INCIDENCIA; ?></h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>T&eacute;cnico</a>
                        </td>
                        <td>
                            <select name="tecnico" style="width: 200px">
                                <?php while ($row = mysql_fetch_assoc($result_tecnico)) { ?>
                                    <option value="<?php echo $row['id_tecnico'] ?>"><?php echo $row['nombre'] ?></option>
                                <?php } ?>       
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Diagn&oacute;stico</a>
                        </td>
                        <td>
                            <input type="text"name="diagnostico" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Tarea realizada</a>
                        </td>
                        <td>
                            <input type="text"name="tareaRealizada" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Estado</a>
                        </td>
                        <td>
                            <input type="hidden" name="id_fichaTecnica" value="<?php echo $idFichaTecnica; ?>">
                            <select name="estado" style="width: 200px">
                                <?php while ($row = mysql_fetch_assoc($result_estado)) { ?>
                                    <option value="<?php echo $row['id_estado']; ?>" selected="1"><?php echo $row['nombre'] ?></option>
                                <?php } ?>       
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Observaciones Incidencia</a>
                        </td>
                        <td>
                            <textarea rows="4" cols="50" style="width:400px; height:100px;"  name="observacionesIncidencia"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Fecha Inicio</a>
                        </td>
                        <td>
                            <input type="text"name="fechaInicio" value="<?php echo $hoy ?>"style="width:200px;">
                            <small>Ej: YYYY-MM-DD HH:MM:SS</small>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Fecha F&iacute;n </a>
                        </td>
                        <td>
                            <input type="text"name="fechaFin" value="<?php echo $hoy ?>" style="width:200px;">
                            <small>Ej: YYYY-MM-DD HH:MM:SS</small>
                        </td>
                    </tr>

                    <tr>
                        <td>

                        </td>
                        <td>
                            <input type="button"  name="boton" value="Cancelar" class="btn" style="float: left" onclick=" location.href = 'javascript:history.back()'" >
                            <button type="submit" name="boton" value="guardar" class="btn btn-success" style="float: right" >Confirmar</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
