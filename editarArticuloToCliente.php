<?php
include './Controllers/securityController.php';
validarSesion();
include './Controllers/connection.php';

$id = filter_input(INPUT_GET, 'id');

$query = "SELECT * FROM articuloCliente AS ac "
        . "INNER JOIN cliente AS c ON ac.id_cliente=c.id_cliente "
        . "INNER JOIN articulo AS a ON ac.id_articulo=a.id_articulo "
        . " WHERE ac.id_articulocliente='$id'";
$result = mysql_query($query);
$row_ac = mysql_fetch_assoc($result);

$query = "SELECT id_cliente,nombre FROM cliente";
$result_cliente = mysql_query($query);

$query = "SELECT * FROM articulo";
$result_articulo = mysql_query($query);
?>

<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>tecss-Cargar Art&iacute;culo</title>
    </head>
    <body>
        <?php include ("./nav.php"); ?>
        <div class="divContenedoraTabla tablaCarga">
            <h2>Editar Art&iacute;culo a Cliente</h2>
            <form name="editarArticuloToCliente" method="post" action="Controllers/editarArticuloToClienteController.php?id=<?php echo $id ?>">
                <table>
                    <tr>
                        <td>
                            <a>Cliente</a>
                        </td>
                        <td>
                            <select name="cliente" style="width: 200px">
                                <?php while ($row = mysql_fetch_assoc($result_cliente)) { ?>
                                    <option value="<?php echo $row['id_cliente'] ?>"  <?php
                                    if ($row['id_cliente'] == $row_ac['id_cliente']) {
                                        echo "selected=true";
                                    }
                                    ?>><?php echo $row['nombre'] ?></option>
<?php } ?>       
                            </select>
                            <a href="cargarCliente.php" target="_blank"><img src="images/add_us.png" border="0" alt="editor" width="20" height="20"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Articulo</a>
                        </td>
                        <td>
                            <select name="articulo" style="width: 200px">
                                <?php while ($row = mysql_fetch_assoc($result_articulo)) { ?>
                                    <option value="<?php echo $row['id_articulo'] ?>" <?php
                                                if ($row['id_articulo'] == $row_ac['id_articulo']) {
                                                    echo "selected=true";
                                                }
                                                ?>>
    <?php echo $row['nombre'] . "-" . $row['marca'] . "-" . $row['modelo'] ?>
                                    </option>
<?php } ?>       
                            </select>
                            <a href="cargarArticulo.php" target="_blank" ><img src="images/add_ar.png" border="0" alt="editor" width="20" height="20"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>N&uacute;mero de serie</a>
                        </td>
                        <td>
                            <input type="text"name="nroSerie" value="<?php echo $row_ac['nroSerie']; ?>" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Observaciones</a>
                        </td>
                        <td>
                            <textarea rows="4" cols="50" style="width:400px; height:100px;" value="<?php echo $row_ac['observaciones']; ?>"  name="observaciones"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                        <td>
                            <input type="button"  name="boton" value="Cancelar" style="float: left" onclick=" location.href = 'javascript:history.back()'" >
                            <button type="submit" name="boton" value="guardar" style="float: right" >Confirmar</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
