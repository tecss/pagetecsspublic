<?php
include_once './Controllers/securityController.php';
include_once './Controllers/connection.php';
include_once './Controllers/FRASES.php';
validarSesion();
$query = "SELECT * FROM cliente ORDER BY nombre ASC";
$result = mysql_query($query);
?>

<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>Tecss - Men&uacute; Clientes</title>
    </head>
    <body>
        <?php include ('./nav.php'); ?>
        <div class="divContenedoraTabla">
            <table class="table table-striped table-generic">
                <thead>
                    <tr>
                        <td>
                            C&oacutedigo
                        </td>
                        <td>
                            Cliente  
                        </td>
                        <td>
                            E-mail
                        </td>
                        <td>
                            Direcci&oacute;n
                        </td>
                        <td>
                            Tel&eacute;fono
                        </td>
                        <td>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    //repetir "crear fila"
                    while ($row = mysql_fetch_assoc($result)) {
                        ?>
                        <tr>
                            <td >
                                <?php echo $row['id_negocio']; ?>
                            </td>
                            <td>
                                <?php echo $row['nombre']; ?>
                            </td> 
                            <td>
                                <?php echo $row['email']; ?>
                            </td>
                            <td>
                                <?php echo $row['direccion']; ?>
                            </td>
                            <td>
                                <?php echo $row['telefono']; ?>
                            </td>
                            <td>
                                <div>
                                    <a href="clienteDetalle.php?id=<?php echo $row['id_cliente']; ?>" class="btn btn-primary"><i class="icon-eye-open icon-white"></i><br/> <?php echo VER;?> </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                    //hasta aca- repetir
                    ?>
                </tbody>
            </table>
        </div>
    </body>
</html>