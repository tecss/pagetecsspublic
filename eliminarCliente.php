<?php
include './Controllers/securityController.php';
include './Controllers/connection.php';
validarSesion();
establecerPermisoA();

$id = filter_input(INPUT_GET, 'id');
$query = "SELECT * FROM cliente WHERE id_cliente='" . $id . "'";
$result = mysql_query($query);
$row = mysql_fetch_assoc($result);
?>
<html>
    <head>
        <title>tecss-Cargar Cliente</title>
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
    </head>
    <body>
        <?php include ("./nav.php"); ?>
        <div class="divContenedoraTabla tablaCarga">
            <div class="alert alert-danger">
            <h2>Eliminar Cliente</h2>
            </div>
            <form name="eliminarCliente" method="post" action="Controllers/eliminarClienteController.php?id=<?php echo $id ?>">
                <table>
                    <tr>
                        <td>
                            <a>ID de negocio</a>
                        </td>
                        <td>
                            <input type="text"name="idNegocio" disabled="true" value="<?php echo $row['id_negocio']; ?>" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Nombre</a>
                        </td>
                        <td>
                            <input type="text"name="nombre" disabled="true" style="width:200px;" value="<?php echo $row['nombre']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>E-mail</a>
                        </td>
                        <td>
                            <input type="text" name="email" disabled="true" style="width:200px;" value="<?php echo $row['email']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Direcci&oacute;n</a>
                        </td>
                        <td>
                            <input type="text"name="direccion" disabled="true" style="width:200px;" value="<?php echo $row['direccion']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Tel&eacute;fono</a>
                        </td>
                        <td>
                            <input type="text"name="telefono" disabled="true" style="width:200px;" value="<?php echo $row['telefono']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Fax</a>
                        </td>
                        <td>
                            <input type="text"name="fax" disabled="true" style="width:200px;" value="<?php echo $row['fax']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Contacto</a>
                        </td>
                        <td>
                            <input type="text" disabled="true" cols="40" rows="5" style="width:400px; height:100px;"  name="contacto" value="<?php echo $row['contacto']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Observaciones</a>
                        </td>
                        <td>
                            <textarea disabled="true" cols="40" rows="5" style="width:400px; height:100px;"  name="observaciones" ><?php echo $row['observaciones']; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                        <td>
                            <input type="button"  name="boton" value="Cancelar" class="btn" style="float: left" onclick=" location.href = 'javascript:history.back()'" >
                            <button type="submit" name="boton" value="guardar" class="btn btn-danger" style="float: right" >Eliminar</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>