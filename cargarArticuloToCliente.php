<?php
include './Controllers/securityController.php';
validarSesion();
include './Controllers/connection.php';

$query = "SELECT id_cliente,nombre FROM cliente";
$result_cliente = mysql_query($query);

$query = "SELECT * FROM articulo";
$result_articulo = mysql_query($query);
?>

<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>tecss-Cargar Articulo</title>
    </head>
    <body>
        <?php include("./nav.php") ?> 
        <div class="divContenedoraTabla tablaCarga">
            <h2>Cargar Art&iacute;culo a Cliente</h2>
            <form name="cargarArticuloToCliente" method="post" action="Controllers/cargarArticuloToClienteController.php">
                <table>
                    <tr>
                        <td>
                            <a>Cliente</a>
                        </td>
                        <td>
                            <select name="cliente" style="width: 200px">
                                <?php while ($row = mysql_fetch_assoc($result_cliente)) { ?>
                                    <option value="<?php echo $row['id_cliente'] ?>"><?php echo $row['nombre'] ?></option>
                                <?php } ?>       
                            </select>
                            <a href="cargarCliente.php" target="_blank"><img src="images/add_us.png" border="0" alt="editor" width="20" height="20"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Art&iacute;culo</a>
                        </td>
                        <td>
                            <select name="articulo" style="width: 200px">
                                <?php while ($row = mysql_fetch_assoc($result_articulo)) { ?>
                                    <option value="<?php echo $row['id_articulo'] ?>">
                                        <?php echo $row['nombre'] . "-" . $row['marca'] . "-" . $row['modelo'] ?>
                                    </option>
                                <?php } ?>       
                            </select>
                            <a href="cargarArticulo.php" target="_blank" ><img src="images/add_ar.png" border="0" alt="editor" width="20" height="20"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>N&uacute;mero de serie</a>
                        </td>
                        <td>
                            <input type="text"name="nroSerie" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Observaciones</a>
                        </td>
                        <td>
                            <textarea rows="4" cols="50" style="width:400px; height:100px;"  name="observaciones"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                        <td>
                            <input type="button"  name="boton" value="Cancelar" class="btn" style="float: left" onclick=" location.href = 'javascript:history.back()'" >
                            <button type="submit" name="boton" value="guardar" class="btn btn-success" style="float: right" >Confirmar</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>