<?php
include_once './Controllers/securityController.php';
include_once './Controllers/connection.php';
include_once './Controllers/FRASES.php';
validarSesion();

$id = filter_input(INPUT_GET, 'id');
$query = "SELECT ft.id_fichaTecnica,ft.fechaEntrada, articulo.id_articulo, cliente.id_cliente, "
        . "ft.fechaSalida,nombre_et,ft.problema AS problema_ft,ft.observaciones AS observaciones_ft ,nombre_cliente,"
        . "nombre_articulo,ft.id_articulocliente, articulo.marca , articulo.modelo "
        . "FROM fichaTecnica AS ft "
        . "INNER JOIN (SELECT id_articulocliente,id_cliente AS id_cliente_ac, id_articulo AS id_articulo_ac FROM articuloCliente ) AS ac "
        . "ON ft.id_articulocliente=ac.id_articulocliente "
        . "INNER JOIN (SELECT id_cliente,nombre AS nombre_cliente FROM cliente ) AS cliente "
        . "ON ac.id_cliente_ac=cliente.id_cliente "
        . "INNER JOIN (SELECT id_estadoFichaTecnica,nombre AS nombre_et FROM estadoFichaTecnica) AS et_ft "
        . "ON et_ft.id_estadoFichaTecnica=ft.estado "
        . "INNER JOIN (SELECT id_articulo,nombre AS nombre_articulo,marca,modelo FROM articulo) AS articulo "
        . "ON ac.id_articulo_ac = articulo.id_articulo "
        . "WHERE ft.id_fichaTecnica='$id'";
$result = mysql_query($query);

$query = "SELECT ft.id_fichaTecnica,ft.fechaEntrada,"
        . "ft.fechaSalida,ft.observaciones AS observaciones_ft,"
        . "i.id_incidencia,i.problema,i.tarea_realizada,i.estado,i.observaciones AS observaciones_i,i.id_tecnico,"
        . "i.id_fichaTecnica,t.nombre AS nombre_tecnico, nombre_et,i.fechaInicio,i.fechaFin "
        . "FROM fichaTecnica AS ft "
        . "INNER JOIN (SELECT * FROM incidencia WHERE fechaDeDestruccion IS NULL) AS i "
        . "ON ft.id_fichaTecnica=i.id_fichaTecnica "
        . "INNER JOIN (SELECT id_estado,nombre AS nombre_et FROM estadoIncidencia) AS et "
        . "ON et.id_estado=i.estado "
        . "INNER JOIN tecnico AS t "
        . "ON i.id_tecnico=t.id_tecnico "
        . "WHERE ft.id_fichaTecnica='$id' ORDER BY fechaFin DESC";
$resultIncidencias = mysql_query($query);
?>

<html>

    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>tecss- Ficha T&eacute;cnica Detalles</title>
    </head>
    <body>
        <?php include ("nav.php"); ?>
        <div class="divContenedoraTabla">
            <h2>Ficha T&eacute;cnica</h2>

            <br/>
            <table class="table table-hover table-generic">
                <thead>
                    <tr>
                        <td>
                            Fecha de Entrada
                        </td>
                        <td>
                            Nombre del Cliente
                        </td>
                        <td>
                            Art&iacute;culo
                        </td>
                        <td>
                            &Uacute;ltima Modificaci&oacute;n
                        </td>
                        <td>
                            Problema
                        </td>
                        <td>
                            Estado
                        </td>
                        <td>
                            Observaciones
                        </td>
                        <td>

                        </td>
                    </tr>
                </thead>
                <?php
                $row = mysql_fetch_assoc($result);
                ?>
                <tbody>
                    <tr>
                        <td>
                            <?php echo date("Y-m-d H:i", strtotime($row['fechaEntrada'])); ?>
                        </td>
                        <td>
                            <a href="clienteDetalle.php?id=<?php echo $row['id_cliente']; ?>"><?php echo $row['nombre_cliente']; ?></a>
                        </td>
                        <td>
                            <a href="articuloDetalle.php?id=<?php echo $row['id_articulo']; ?>"><?php echo $row['nombre_articulo'] . " - " . $row['marca'] . " - " . $row['modelo']; ?></a>
                        </td>
                        <td>
                            <?php echo date("Y-m-d H:i", strtotime($row['fechaSalida'])); ?>
                        </td>
                        <td>
                            <?php echo $row['problema_ft']; ?>
                        </td>
                        <td>
                            <?php echo $row['nombre_et']; ?>
                        </td>
                        <td>
                            <?php echo $row['observaciones_ft']; ?>
                        </td>
                        <td>
                            <div class="btn-group pull-right">
                                <a href="editarFichaTecnica.php?id=<?php echo $id ?>" class="btn btn-group"><i class="icon-pencil"></i> <?php echo EDITAR; ?></a>
                                <a href="eliminarFichaTecnica.php?id=<?php echo $id ?>" class="btn btn-danger btn-group"><i class="icon-trash icon-white"></i> <?php echo ELIMINAR; ?></a>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>

            <h2>Tareas</h2>

            <table class="table table-hover table-generic">
                <thead>
                    <tr>
                        <td>
                            Diagn&oacute;stico
                        </td>
                        <td>
                            Tarea realizada
                        </td>
                        <td>
                            Estado
                        </td>
                        <td>
                            T&eacute;cnico
                        </td>
                        <td>
                            Observaciones
                        </td>
                        <td>
                            Inicio
                        </td>
                        <td>
                            F&iacute;n
                        </td>
                        <td>
                            <a  href="agregarIncidencia.php?id_fichaTecnica=<?php echo $id; ?>" class="pull-right btn btn-primary"><i class="icon-plus icon-white"></i> <?php echo AGREGAR_INCIDENCIA; ?></a>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    //repetir "crear fila"
                    while ($row = mysql_fetch_assoc($resultIncidencias)) {
                        ?>
                        <tr>
                            <td>
                                <?php echo $row['problema']; ?>
                            </td>
                            <td>
                                <?php echo $row['tarea_realizada']; ?>
                            </td>
                            <td>
                                <?php echo $row['nombre_et']; ?>
                            </td>
                            <td>
                                <?php echo $row['nombre_tecnico']; ?>
                            </td>
                            <td>
                                <?php echo $row['observaciones_i']; ?>
                            </td>
                            <td>
                                <?php echo date("Y-m-d H:i", strtotime($row['fechaInicio'])); ?>
                            </td>
                            <td>
                                <?php echo date("Y-m-d H:i", strtotime($row['fechaFin'])); ?>
                            </td>
                            <td>
                                <div class="btn-group pull-right">
                                    <a href="imprimirIncidencia.php?id=<?php echo $row['id_incidencia']; ?>" class="btn btn-info btn-group"><i class="icon-print icon-white"></i> <?php echo IMPRIMIR; ?> </a>
                                    <a href="editarIncidencia.php?id=<?php echo $row['id_incidencia'] ?>" class="btn btn-group"><i class="icon-pencil"></i> <?php echo EDITAR; ?> </a>
                                    <a href="eliminarIncidencia.php?id=<?php echo $row['id_incidencia']; ?>" class="btn btn-danger btn-group"><i class="icon-trash icon-white"></i> <?php echo ELIMINAR; ?> </a>
                                </div>
                            </td>

                        </tr>
                        <?php
                    }
                    //hasta aca- repetir
                    ?>
                </tbody>
            </table>
        </div>
    </body>
</html>

