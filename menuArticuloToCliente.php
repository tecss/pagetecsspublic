<?php
include './Controllers/securityController.php';
include './Controllers/connection.php';
validarSesion();

$idArticulo = filter_input(INPUT_GET, 'id');
$query = "SELECT a.nombre ,a.marca,a.modelo FROM articuloCliente AS ac "
        . "INNER JOIN articulo AS a ON ac.id_articulo=a.id_articulo "
        . " WHERE ac.id_articulo='$idArticulo'";
$resultArt = mysql_query($query);
$rowArt = mysql_fetch_assoc($resultArt);
$query = "SELECT c.nombre,ac.nroSerie,ac.observaciones,ac.id_articulocliente FROM articuloCliente AS ac "
        . "INNER JOIN cliente AS c ON ac.id_cliente=c.id_cliente "
        . "INNER JOIN articulo AS a ON ac.id_articulo=a.id_articulo "
        . " WHERE ac.id_articulo='$idArticulo'";
$result = mysql_query($query);
?>

<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>tecss - Men&uacute; Art&iacute;culo</title>
    </head>
    <body>
        <div id="espacioSeparador30h"><br/></div>
        <table width="100%">
            <tr>
                <td>
                    <a style="margin-left: 2%" href="cargarArticulo.php"><img src="images/docs.png" border="0" alt="editor" width="20" height="20">Nuevo Art&iacute;culo Generico</a>
                </td>
                <td>
                    <a style="float: right;padding-right: 10%"  href="index.php">Inicio</a>
                </td>
            </tr>
            <tr>
                <td>
                    <br/>
                    <a style="margin-left: 2%" href="cargarArticuloToCliente.php"><img src="images/id_pencil.png" border="0" alt="editor" width="20" height="20">Nuevo Articulo de cliente (especifico)</a>

                </td>
            </tr>
        </table>
        <h1>Art&iacute;culo - <?php echo $rowArt['nombre'] . "-" . $rowArt['marca'] . " - " . $rowArt['modelo']; ?></h1>
        <div id="espacioSeparador30h"><br/></div>
        <table width="100%" id="ver-minimalist">
            <tr>
                <td>
                    <h3>
                        Cliente
                    </h3>
                </td>
                <td>
                    <h3>
                        Nro de Serie
                    </h3>
                </td>
                <td>
                    <h3>
                        Observaciones
                    </h3>
                </td>
                <td>
                    <h3>
                        Operaciones
                    </h3>
                </td>
            </tr>
            <?php
//repetir "crear fila"
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <tr>
                    <td>
                        <?php echo $row['nombre']; ?>
                    </td>
                    <td>
                        <?php echo $row['nroSerie']; ?>
                    </td>
                    <td>
                        <?php echo $row['observaciones']; ?>
                    </td>
                    <td>
                        <div style="padding-left:10%">
                            <a href="editarArticuloToCliente.php?id=<?php echo $row['id_articulocliente']; ?>"><img src="images/gnome_editor.png" border="0" alt="editor" width="30" height="30"></a>
                            <a href="eliminarArticuloToCliente.php?id=<?php echo $row['id_articulocliente']; ?>"><img src="images/gnome_delete.png" border="0" alt="delete" width="30" height="30"></a>
                        </div>
                    </td>
                </tr>
                <?php
            }
//hasta aca- repetir
            ?>
        </table>

    </body>
</html>