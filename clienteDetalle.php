<?php
include './Controllers/securityController.php';
include './Controllers/connection.php';
include './Controllers/FRASES.php';
validarSesion();

$id = filter_input(INPUT_GET, 'id');
$query = "SELECT * FROM cliente "
        . "INNER JOIN (SELECT * FROM clienteAbono WHERE fechaFin IS NULL) AS ca ON ca.id_cliente=cliente.id_cliente "
        . "INNER JOIN (SELECT id_abono,nombre AS nombre_ab FROM abono) AS ab ON ab.id_abono=ca.id_abono "
        . "WHERE cliente.id_cliente='" . $id . "' ORDER BY ca.fechaInicio DESC";
$result = mysql_query($query);
$row = mysql_fetch_assoc($result);
$query = "SELECT a.nombre,a.marca,a.modelo,ac.nroSerie,ac.observaciones,ac.id_articulocliente FROM articuloCliente AS ac "
        . "INNER JOIN cliente AS c ON ac.id_cliente=c.id_cliente "
        . "INNER JOIN articulo AS a ON ac.id_articulo=a.id_articulo "
        . " WHERE ac.id_cliente='$id'";
$result_ac = mysql_query($query);
?>

<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>Men&uacute; Clientes</title>
    </head>
    <body>
        <?php include ("nav.php"); ?>
        <div class="divContenedoraTabla">
            <h2>Cliente <?php echo $row['nombre']; ?></h2>
            <table class="table table-hover table-generic">
                <thead>
                    <tr>
                        <td>
                            id Negocio
                        </td>
                        <td>
                            Nombre   
                        </td>
                        <td>
                            Email
                        </td>
                        <td>
                            Direcci&oacute;n
                        </td>
                        <td>
                            Tel&eacute;fono
                        </td>
                        <td>
                            Fax
                        </td>
                        <td>
                            Contacto
                        </td>
                        <td>
                            Abono
                        </td>
                        <td>
                            Observaciones
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php echo $row['id_negocio']; ?>
                        </td>
                        <td>
                            <?php echo $row['nombre']; ?>
                        </td>
                        <td>
                            <?php echo $row['email']; ?>
                        </td>
                        <td>
                            <?php echo $row['direccion']; ?>
                        </td>
                        <td>
                            <?php echo $row['telefono']; ?>
                        </td>
                        <td>
                            <?php echo $row['fax']; ?>
                        </td>
                        <td>
                            <?php echo $row['contacto']; ?>
                        </td>
                        <td>
                            <?php echo $row['nombre_ab']; ?>
                        </td>
                        <td>
                            <?php echo $row['observaciones']; ?>
                        </td>
                        <td>
                            <div class="btn-group pull-right">
                                <a href="editarCliente.php?id=<?php echo $row['id_cliente']; ?>" class="btn btn-group"><i class="icon-pencil "></i> <?php echo EDITAR; ?></a>
                                <a href="eliminarCliente.php?id=<?php echo $row['id_cliente']; ?>" class="btn btn-danger btn-group"><i class="icon-trash icon-white"></i> <?php echo ELIMINAR; ?></a>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <h2>Art&iacute;culos de <?php echo $row['nombre']; ?></h2>
            <table class="table table-hover table-generic">
                <thead>
                    <tr>
                    <tr>
                        <td>
                            Nombre Art&iacute;culo
                        </td>
                        <td>
                            Nro de Serie
                        </td>
                        <td>
                            Observaciones
                        </td>
                        <td>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php
//repetir "crear fila"
                    while ($row_ac = mysql_fetch_assoc($result_ac)) {
                        ?>
                        <tr>
                            <td>
                                <?php echo $row_ac['nombre'] . "-" . $row_ac['marca'] . " - " . $row_ac['modelo']; ?>
                            </td>
                            <td>
                                <?php echo $row_ac['nroSerie']; ?>
                            </td>
                            <td>
                                <?php echo $row_ac['observaciones']; ?>
                            </td>
                            <td>
                                <div class="btn-group pull-right">
                                    <a href="editarArticuloToCliente.php?id=<?php echo $row_ac['id_articulocliente']; ?>" class="btn btn-group"><i class="icon-pencil"></i> <?php echo EDITAR; ?> </a>
                                    <a href="eliminarArticuloToCliente.php?id=<?php echo $row_ac['id_articulocliente']; ?>" class="btn btn-danger btn-group"><i class="icon-trash icon-white"></i> <?php echo ELIMINAR; ?> </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
//hasta aca- repetir
                    ?>
                </tbody>
            </table>
        </div>
    </body>
</html>