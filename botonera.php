<?php $menu = filter_input(INPUT_GET, 'menu');
$carga = filter_input(INPUT_GET, 'carga'); ?>
<div class="divContenedoraTabla tablaCarga" >
    <div>
        <salir  class="botonera" style="float: right;font-size: 1.2em"><?php echo $_SESSION['user'] ?> - <a href="Controllers/cerrarSesionController.php" class=" botonera cerraSesion" >Cerrar sesi&oacute;n </a></salir>
        <h1 style="font-family: Ubuntu"><img src="images/kcmpci.png" style="margin-bottom: -20px" border="0" alt="delete" width="50" height="50">Canoffice.es</h1>
    </div>
</div>
<div  style="background: #f5f5f5; box-shadow: 2px 2px 5px #999;">
    <div class="divContenedoraTabla tablaCarga" style="background: #f5f5f5;">
        <table width="100%">
            <tr>
                <td>
                    <a href="menuCliente.php?menu=cliente" class="botonera <?php if ($menu == 'cliente' || $carga == 'cliente') {echo 'active';}?>" >Clientes <palo style="color: #2a85a1; ">|</palo></a>
                    <a href="menuFichaTecnica.php?menu=fichaTecnica" class="botonera <?php if ($menu == 'fichaTecnica' || $carga == 'fichaTecnica') {echo 'active';} ?>">Fichas T&eacute;cnicas <palo style="color: #2a85a1">|</palo></a>
                    <a href="menuArticulo.php?menu=articulo" class="botonera <?php if ($menu == 'articulo' || $carga == 'articulo') {echo 'active';} ?>">Art&iacute;culos <palo style="color: #2a85a1">|</palo></a>
                    <a href="menuTecnico.php?menu=tecnico" class="botonera <?php if ($menu == 'tecnico' || $carga == 'Tecnico') {echo 'active';} ?>">T&eacute;cnicos <palo style="color: #2a85a1">|</palo></a></a>
                    <a href="menuReportes.php?menu=reporte" class="botonera <?php if ($menu == 'reporte') {echo 'active';} ?>">Reportes </a>
                </td>
                <td>
                    <a style="float: right;padding-right: 10%;" class="botonera <?php if ($menu == 'inicio' ) {echo 'active';} ?>"  href="index.php?menu=inicio" >Inicio</a>
                </td>
            </tr>
<?php if ($menu == 'cliente') { ?>
                <tr><td colspan="2"><a href="cargarCliente.php" class="botonera" ><img src="images/docs.png" border="0" alt="editor" width="20" height="20">Nuevo Cliente </a> </td></tr>

<?php } elseif ($menu == 'fichaTecnica') { ?>
                <tr>
                    <td>
                        <a href="cargarFichaTecnica.php" class="botonera" ><img src="images/docs.png" border="0" alt="editor" width="20" height="20">Nueva Ficha T&eacute;cnica </a>
                    </td>
                    <td>
                        <FORM METHOD=POST ACTION="./menuFichaTecnica.php?menu=fichaTecnica" style="float: right;padding-right: 10%;margin: 0px 0px 0px 0px"> 
                            <INPUT TYPE="text" NAME="busqueda" class="input-large search-query" value="<?php echo $busqueda;?>"> <!-- la variable $busqeda esta definida en el archivo menuFichaTecnica y representa la busqueda que se acaba de hacer-->
                            <INPUT TYPE="submit" VALUE="Buscar" class="btn">
                        </FORM>
                    </td>
                </tr>        
                        <?php } elseif ($menu == 'articulo') { ?>
                <tr>
                    <td colspan="2"> 
                        <a href="cargarArticulo.php" class="botonera "><img src="images/docs.png" border="0" alt="editor" width="20" height="20">Nuevo Art&iacute;culo </a>
                    </td>
                </tr>
<?php } elseif ($menu == 'tecnico') { ?>

                <tr> <td colspan="2"><a  href="cargarTecnico.php" class="botonera"><img src="images/docs.png" border="0" alt="editor" width="20" height="20">Nuevo T&eacute;cnico</a></td> </tr>
<?php } elseif ($menu == 'fichaTecnicaDetalle') { ?>

                <tr> <td colspan="2"><a  href="agregarIncidencia.php?id_fichaTecnica=<?php echo $id;// $id se refiere a una variable en menuFichaTecnica.php, que contiene el id de la fichaTecnica?>" class="botonera"><img src="images/docs.png" border="0" alt="editor" width="20" height="20">Agregar Incidencia</a></td> </tr>
<?php } ?>
        </table>
    </div>
</div>