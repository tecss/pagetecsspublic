<?php
include './Controllers/securityController.php';
include './Controllers/connection.php';
include_once './Controllers/FRASES.php';
validarSesion();


$id = filter_input(INPUT_GET, 'id');

$query = "SELECT "
        . "i.id_incidencia,i.problema,i.tarea_realizada,i.estado,i.observaciones AS observaciones_i,t.id_tecnico_t,"
        . "i.id_fichaTecnica,nombre_t , i.fechaInicio , i.fechaFin "
        . "FROM incidencia AS i "
        . "INNER JOIN (SELECT id_tecnico AS id_tecnico_t,nombre AS nombre_t FROM tecnico ) AS t "
        . "ON i.id_tecnico=t.id_tecnico_t "
        . "WHERE i.id_incidencia='$id'";
$resultIncidencias = mysql_query($query);

$query = "SELECT id_cliente,nombre FROM cliente";
$result_cliente = mysql_query($query);

$query = "SELECT id_articulo,nombre,marca,modelo FROM articulo";
$result_articulo = mysql_query($query);

$query = "SELECT id_estado,nombre FROM estadoIncidencia ";
$result_estado = mysql_query($query);

$hoy = date("Y-m-d H:i");
?>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>Editar incidencia</title>
    </head>
    <body>
        <?php include ("./nav.php"); ?>
        <div class="divContenedoraTabla tablaCarga">
            <h2>Editar incidencia</h2>
            <form name="editarIncidencia" method="post" action="Controllers/editarIncidenciaController.php?id=<?php echo $id ?>">
                <table>
                    <tr>
                        <td>
                            <h2><?php echo INCIDENCIA; ?></h2>
                        </td>
                    </tr>
                    <?php
                    $rowInci = mysql_fetch_assoc($resultIncidencias);
                    ?>
                    <tr>
                        <td>
                            <a>Tecnico</a>
                        </td>
                        <td>
                            <select name="tecnico" readOnly="1" style="width: 200px">
                                <option value="<?php echo $rowInci['id_tecnico_t'] ?>"  selected=true >
                                    <?php echo $rowInci['nombre_t'] ?>
                                </option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Diagn&oacute;stico</a>
                        </td>
                        <td>
                            <input type="text"name="diagnostico" value="<?php echo $rowInci['problema']; ?>" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Tarea realizada</a>
                        </td>
                        <td>
                            <input type="text" name="tareaRealizada" value="<?php echo $rowInci['tarea_realizada']; ?>" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Estado</a>
                        </td>
                        <td>
                            <input type="hidden" name="id_fichaTecnica" value="<?php echo $rowInci['id_fichaTecnica']; ?>">
                            <input type="hidden" name="id" value="<?php echo $rowInci['id_incidencia']; ?>">
                            <select name="estado" style="width: 200px">
                                <?php while ($row = mysql_fetch_assoc($result_estado)) { ?>
                                    <option value="<?php echo $row['id_estado']; ?>" <?php
                                    if ($row['id_estado'] == $rowInci['estado']) {
                                        echo "selected=true";
                                    }
                                    ?>>
                                                <?php echo $row['nombre'] ?>
                                    </option>
                                <?php } ?>       
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Observaciones Incidencia</a>
                        </td>
                        <td>
                            <textarea rows="4" cols="50" style="width:400px; height:100px;"  name="observacionesIncidencia"><?php echo $rowInci['observaciones_i']; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Fecha Inicio</a>
                        </td>
                        <td>
                            <input type="text"name="fechaInicio" value="<?php echo date("Y-m-d H:i",strtotime($rowInci['fechaInicio'])); ?>"style="width:200px;">
                            <small>Ej: YYYY-MM-DD HH:MM:SS</small>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Fecha F&iacute;n </a>
                        </td>
                        <td>
                            <input type="text"name="fechaFin" value="<?php echo date("Y-m-d H:i",strtotime($rowInci['fechaFin'])); ?>" style="width:200px;">
                            <small>Ej: YYYY-MM-DD HH:MM:SS</small>
                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                        <td>
                            <input type="button"  name="boton" value="Cancelar" class="btn" style="float: left" onclick=" location.href = 'javascript:history.back()'" >
                            <button type="submit" name="boton" value="guardar" class="btn btn-warning" style="float: right" >Confirmar</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
