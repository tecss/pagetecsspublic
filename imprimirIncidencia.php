<?php
include './Controllers/securityController.php';
include './Controllers/connection.php';
validarSesion();
$id = filter_input(INPUT_GET, 'id');

$query = "SELECT c.nombre AS c_nombre,c.direccion AS c_direccion,c.email AS c_email ,c.telefono AS c_telefono"
        . ",a.nombre AS a_nombre,a.marca AS a_marca,a.modelo AS a_modelo, ac.nroSerie AS ac_nroSerie "
        . ", i.problema AS i_problema , t.nombre AS t_nombre, i.tarea_realizada AS i_tarea_realizada,"
        . " i.observaciones AS i_observaciones, i.fechaInicio, i.fechaFin,nombre_et,ft.id_fichaTecnica,ft.fechaEntrada,"
        . "ft.fechaSalida,ft.observaciones AS observaciones_ft,ft.problema AS problema_ft,ft.id_negocio "
        . "FROM incidencia AS i "
        . "INNER JOIN fichaTecnica AS ft "
        . "ON i.id_fichaTecnica=ft.id_fichaTecnica "
        . "INNER JOIN articuloCliente AS ac "
        . "ON ac.id_articulocliente=ft.id_articulocliente "
        . "INNER JOIN cliente AS c "
        . "ON c.id_cliente=ac.id_cliente "
        . "INNER JOIN articulo AS a "
        . "ON a.id_articulo=ac.id_articulo "
        . "INNER JOIN (SELECT id_estado,nombre AS nombre_et FROM estadoIncidencia) AS et "
        . "ON et.id_estado=i.estado "
        . "INNER JOIN tecnico AS t "
        . "ON i.id_tecnico=t.id_tecnico "
        . "WHERE id_incidencia='$id'";
$resultIncidencias = mysql_query($query);
$row = mysql_fetch_assoc($resultIncidencias);

$fechaInicio= strtotime($row['fechaInicio']);
$fechaFin= strtotime($row['fechaFin']);
$duracion = mktime(date('H',$fechaFin),date('i',$fechaFin),date('s',$fechaFin),date('n',$fechaFin),date('j',$fechaFin),date('Y',$fechaFin))-mktime(date('H',$fechaInicio),date('i',$fechaInicio),date('s',$fechaInicio),date('n',$fechaInicio),date('j',$fechaInicio),date('Y',$fechaInicio));
$duracion = abs($duracion);
$horasdiferencia=floor(($duracion/60)/60); //Redondeamos con floor
$minutosdiferencia=((($duracion/60)/60)-floor(($duracion/60)/60))*60;
$duracion = $horasdiferencia." horas,".$minutosdiferencia . " min";
$fechaInicio=date("d-m-Y H:i", $fechaInicio);
$fechaFin=date("d-m-Y H:i", $fechaFin);



?>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>Imprimir incidencia</title>
		<style type="text/css"> 
		</style> 
	</head>

    <body>
        <form name="imprimirIncidencia" method="post" action="#">
            <div style="width:100%">
			
                <table>
                    <tr>
					
                        <td rowspan="12" colspan="2">
						<td style= "width: 50%;">
                            <img src="images/can.png" style="" border="0" alt="canoffice" width="100%" height="">
                        </td>
						
						<td style= "font-size:10px;width: 50%;">
								<br><br>
								<DIV ALIGN=Right>
									<strong>C a n O f f i c e . e s</strong><br>
									<em>Milton Terreno Cif:X8510007F<br>
									Los Pedacitos 41 HERMIGUA<br>
									Tel&eacutefono /Fax 922 880 720<br>
									s a t @ c a n o f f i c e . e s
									</em>
								</DIV>
								<br><br><br><br>
								<DIV ALIGN=Right>
                                                                    <h1> Ficha T&eacute;:cnica N 2014-00 <?php echo $row['id_negocio'] ?></h1>
								</DIV>
						</td>
					</tr>
				</table>	
			</div>		
                    
			 
				<table>
					<tr>
                        <td rowspan="5" colspan="2" style= "font-size:11px;width: 60%;">
                           	<br><br>							                       
							<a style="font-size: 11px;"><b> Cliente:</b></a><br>
							<input type="text"name="nombre" readonly style="width:70%;font-size: 15px;" value="<?php echo $row['c_nombre']; ?>"><br>
							<input type="text" name="direccion" readonly style="width:70%;font-size: 12px;" value="<?php echo $row['c_direccion']; ?>"><br>
                            <input type="text" name="telefono" readonly style="width:70%;font-size: 12px;" value="<?php echo $row['c_telefono']; ?>"><br>
							<input type="text" name="email" readonly style="width:70%;font-size: 12px" value="<?php echo $row['c_email']; ?>"><br>		
						</td>
						<td style="font-size:11px;width: 40%;">
							<br><br><br>
							<DIV ALIGN=Right>
								<a style="font-size: 11px;"><b> Tiempo:</b></a><br>
									<b>Iniciaci&oacute;n:
									<input type="text" style="width: 40%;font-size: 12px;" name="dd" readonly value="<?php echo $fechaFin ?>"><br>
									Finalizaci&oacute;n:
									<input type="text" style="width: 40%;font-size: 12px;" name="dd" readonly value="<?php echo $fechaFin ?>"><br>
									Duraci&oacute;n:
									<input type="text" style="width: 40%;font-size: 12px;" name="dd" readonly value="<?php echo $duracion ?>"><br>
									<a style="font-size: 11px;"><b> Modalidad:</b></a><br>
									Pago
									<input type="text" name="nombre"  value="" style="width:4%;">
									Pre-Pago
									<input type="text" name="nombre"  value="" style="width:4%;">
									Garant&iacute;a
									<input type="text" name="nombre"  value="" style="width:4%;"></b>
							</DIV>
                        </td>
					</tr>
				</table>
            
					
				<table>
                    <tr >
						<td rowspan="4" colspan="2">
						 <td style="font-size: 11px; width:30% ; ">
						<br><br><br><br><br><br>
						<b>Datos del Art&iacute;culo</b></a>
						<br>
						<a><input type="text"name="nombre" readonly value="<?php echo $row['a_nombre']; ?>" style="width:100%;"></a><br>
                        <a><input type="text" name="marca" readonly value="<?php echo $row['a_marca']; ?>" style="width:100%;"></a><br>
						<a><input type="text"name="modelo" readonly value="<?php echo $row['a_modelo']; ?>" style="width:100%;"></a><br>
                        <a><input type="text"name="nroSerie" readonly value="<?php echo $row['ac_nroSerie']; ?>" style="width:100%;"></a><br>
						</td>
						
						<td style="font-size: 11px; width:70%; position: relative; left:50px ;">
						
						<a><b> Problema </b> </a>
						<br>
						
						<DIV ALIGN=Right>
                        <input type="text" readonly name="problema" value="<?php echo $row['problema_ft']; ?>" style="width:100%;">
						</div>
                        </td>
					</tr>
				</table>
                 
					
                    <tr>
					<td>
					<br><br>
					<a style="font-size: 10px;"><b> Diagn&oacute;stico</b></a><br>  
					
					</td>
					</tr>
                    <tr>
                        <td colspan="4">
                           <?php echo $row['i_problema']; ?><br><br><br></td>
						   </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td><a style="font-size: 10px;"><b> Tareas Realizadas</b></a><br></td></tr>
                    <tr>
                        <td colspan="4">
                            <?php echo $row['i_tarea_realizada']; ?><br><br><br></td></tr>
                            </table>
                        </td>
                    </tr>
                   
                    <tr><td><a style="font-size: 10px;"><b> Piezas a&ntilde;adidas</b></a><br></td></tr>
                    <tr>
                        <td colspan="4">
                            <table border="1" style="width: 100%;border: 0px solid black;border-collapse: collapse;">
                                <tr><td style="border: 1px solid grey;">_</td></tr>
                                <tr><td style="border: 1px solid grey;">_</td></tr>
                                <tr><td style="border: 1px solid grey;">_</td></tr>
                            </table>
							<br><br><br>
                        </td>
                    </tr>                    
                    <tr>
                        <td colspan="2">
						<td style="font-size: 11px; width:50% ; ">
						<br><br><br>
                            <a>Firma del Cliente:</a>
                        </td>
                        <td colspan="2">
						<td style="font-size: 11px; width:50% ; ">
						<br><br><br><br>
                            <a> Firma de T&eacute;cnico:</a>
                            <input type="text"name="telefono" disabled="true" style="" value="<?php echo $row['t_nombre']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            
                        </td>
                        <td colspan="2">
                            
                        </td>
                    </tr>


                    
                </table>    
            </div>
            <br/>
            <br/>
            
        </form>
    </body>
</html>
