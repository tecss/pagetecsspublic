
<?php
$menu = filter_input(INPUT_GET, 'menu');
$carga = filter_input(INPUT_GET, 'carga');
?>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.min.css" rel="stylesheet" type="text/css"> 
        <LINK href="css/style.css" rel="stylesheet" type="text/css"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap-responsive.css" rel="stylesheet">
    </head>
    <body>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#" style="font-family: small-caps">Canoffice.es</a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                        <ul class="nav">
                            <li><a href="index.php"><i class="icon-home"></i>Home</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Ficha T&eacute;cnica<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li class="nav-header">Fichas T&eacute;cnicas</li>
                                    <li><a href="cargarFichaTecnica.php">Agregar</a></li>
                                    <li><a href="menuFichaTecnica.php?menu=fichaTecnica">Explorar</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cliente<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li class="nav-header">Clientes</li>
                                    <li><a href="cargarCliente.php">Agregar</a></li>
                                    <li><a href="menuCliente.php?menu=cliente">Explorar</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Art&iacute;culo<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li class="nav-header">Art&iacute;culos</li>
                                    <li><a href="cargarArticulo.php">Agregar</a></li>
                                    <li><a href="menuArticulo.php?menu=articulo">Explorar</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">T&eacute;cnico<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li class="nav-header">T&eacute;cnico</li>
                                    <li><a href="cargarTecnico.php">Agregar</a></li>
                                    <li><a href="menuTecnico.php?menu=tecnico">Explorar</a></li>
                                </ul>
                            </li>
                            <li><a href="menuReportes.php">Reportes</a></li>
                        </ul>
                        <ul class="nav pull-right">
                            <form class="navbar-search pull-left"  METHOD=POST ACTION="./menuFichaTecnica.php?menu=fichaTecnica">
                                <input type="text" name="busqueda" class="search-query" style="height: 30px" placeholder="Search">
                            </form>
                            <li class="divider-vertical"></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> <?php echo $_SESSION['user'] ?> <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="menuTecnico.php">Perfil</a></li>
                                    <li class="divider"></li>
                                    <li><a href="Controllers/cerrarSesionController.php">Cerrar Sesi&oacute;n</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!-- /.nav-collapse -->
                </div>
            </div><!-- /navbar-inner -->
        </div><!-- /navbar -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-dropdown.js"></script>
        <script type="text/javascript" src="js/bootstrap-collapse.js"></script>
    </body>

</html>