<?php
include_once './Controllers/securityController.php';
include_once './Controllers/connection.php';
include_once './Controllers/FRASES.php';
validarSesion();
$id = filter_input(INPUT_GET, 'id');
$query = "SELECT * FROM articulo WHERE id_articulo='" . $id . "'";
$result = mysql_query($query);
$row = mysql_fetch_assoc($result);

$query = "SELECT c.nombre,ac.nroSerie,ac.observaciones,ac.id_articulocliente FROM articuloCliente AS ac "
        . "INNER JOIN cliente AS c ON ac.id_cliente=c.id_cliente "
        . "INNER JOIN articulo AS a ON ac.id_articulo=a.id_articulo "
        . " WHERE ac.id_articulo='$id'";
$result_ac = mysql_query($query);
?>

<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.min.css" rel="stylesheet" type="text/css"> 
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>tecss - Men&uacute; Art&iacute;culo</title>
    </head>
    <body>
        <?php include ("nav.php"); ?>
        <div class="divContenedoraTabla">
            <h2>Art&iacute;culo - <?php echo $row['nombre'] . "-" . $row['marca'] . " - " . $row['modelo']; ?></h2>
            <table class="table table-hover table-generic">
                <thead>
                    <tr>
                        <td>
                            Nombre   
                        </td>
                        <td>
                            Marca
                        </td>
                        <td>
                            Modelo   
                        </td>
                        <td>
                            Especificaciones
                        </td>
                        <td>
                            Link del fabricante 
                        </td>
                        <td>
                            Link del PDF 
                        </td>
                        <td>
                            Link de Drivers
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php echo $row['nombre']; ?>
                        </td>
                        <td>
                            <?php echo $row['marca']; ?>
                        </td>
                        <td>
                            <?php echo $row['modelo']; ?>
                        </td>
                        <td>
                            <?php echo $row['especificaciones']; ?>
                        </td>
                        <td>
                            <a href="<?php echo $row['link_fabricante']; ?>"><?php echo $row['link_fabricante']; ?></a>
                        </td>
                        <td>
                            <a href="<?php echo $row['link_pdf']; ?>"><?php echo $row['link_pdf']; ?></a>
                        </td>
                        <td>
                            <a href="<?php echo $row['link_drivers']; ?>"><?php echo $row['link_drivers']; ?></a>
                        </td>
                        <td>
                            <?php echo $row['observaciones']; ?>
                        </td>
                        <td>
                            <div class="btn-group">
                                <a href="editarArticulo.php?id=<?php echo $row['id_articulo']; ?>" class="btn btn-group"><i class="icon-pencil"></i> <?php echo EDITAR; ?></a>
                                <a href="eliminarArticulo.php?id=<?php echo $row['id_articulo']; ?>" class="btn btn-danger btn-group"><i class="icon-trash icon-white"></i> <?php echo ELIMINAR; ?></a>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <h2>Registros - <?php echo $row['nombre'] . "-" . $row['marca'] . " - " . $row['modelo']; ?></h2>
            <table class="table table-hover table-generic">
                <thead>
                    <tr>
                        <td>
                            Cliente
                        </td>
                        <td>
                            Nro de Serie
                        </td>
                        <td>
                            Observaciones
                        </td>
                        <td>
                            Operaciones
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php
//repetir "crear fila"
                    while ($row_ac = mysql_fetch_assoc($result_ac)) {
                        ?>
                        <tr>
                            <td>
                                <?php echo $row_ac['nombre']; ?>
                            </td>
                            <td>
                                <?php echo $row_ac['nroSerie']; ?>
                            </td>
                            <td>
                                <?php echo $row_ac['observaciones']; ?>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a href="editarArticuloToCliente.php?id=<?php echo $row_ac['id_articulocliente']; ?>" class="btn btn-group"><i class="icon-pencil"></i> <?php echo EDITAR; ?></a>
                                    <a href="eliminarArticuloToCliente.php?id=<?php echo $row_ac['id_articulocliente']; ?>" class="btn btn-danger btn-group"><i class="icon-trash icon-white"></i> <?php echo ELIMINAR; ?></a>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
//hasta aca- repetir
                    ?>
                </tbody>
            </table>
        </div>
    </body>
</html>
