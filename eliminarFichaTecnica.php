<?php
include './Controllers/securityController.php';
include './Controllers/connection.php';
validarSesion();
establecerPermisoA();
$id = filter_input(INPUT_GET, 'id');
$query = "SELECT ft.id_fichaTecnica,ft.fechaEntrada,id_cliente_ac ,ft.id_articulocliente ,"
        . "ft.fechaSalida,ft.observaciones AS observaciones_ft ,nombre_cliente,"
        . "nombre_articulo,ft.id_articulocliente,ft.estado,ft.problema "
        . "FROM fichaTecnica AS ft "
        . "INNER JOIN (SELECT id_articulocliente,id_cliente AS id_cliente_ac, id_articulo AS id_articulo_ac FROM articuloCliente ) AS ac "
        . "ON ft.id_articulocliente=ac.id_articulocliente "
        . "INNER JOIN (SELECT id_cliente,nombre AS nombre_cliente FROM cliente ) AS cliente "
        . "ON ac.id_cliente_ac=cliente.id_cliente "
        . "INNER JOIN (SELECT id_articulo,nombre AS nombre_articulo FROM articulo) AS articulo "
        . "ON ac.id_articulo_ac = articulo.id_articulo "
        . "WHERE ft.id_fichaTecnica='$id'";
$resultFicha = mysql_query($query);

$rowFicha = mysql_fetch_assoc($resultFicha);
$id_cliente = $rowFicha['id_cliente_ac'];

$query = "SELECT ac.id_articulocliente,ac.id_articulo,articulo.nombre,articulo.marca,articulo.modelo FROM articuloCliente AS ac "
        . "INNER JOIN (SELECT * FROM articulo) AS articulo "
        . "ON ac.id_articulo = articulo.id_articulo "
        . "WHERE id_cliente='$id_cliente'";
$result_articulo = mysql_query($query);

$query = "SELECT id_cliente,nombre FROM cliente";
$result_cliente = mysql_query($query);

$query = "SELECT id_estado,nombre FROM estadoIncidencia ";
$result_estado = mysql_query($query);
$hoy = date("Y-m-d H:i:s");
?>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>Eliminar Ficha T&eacute;cnica</title>
    </head>
    <body>
        <?php include ("./nav.php"); ?>
        <div class="divContenedoraTabla tablaCarga">
            <div class="alert alert-danger">
                <h2>Eliminar Ficha T&eacute;cnica</h2>
                <small>Va a eliminar todas las incidencias correspondientes a esta Ficha tecnica</small>
            </div>
            <form name="eliminarFichaTecnica" method="post" action="Controllers/eliminarFichaTecnicaController.php?id=<?php echo $id ?>">
                <table>
                    <tr>
                        <td>
                            <h2>Ficha T&eacute;cnica</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Fecha de Entrada</a>
                        </td>
                        <td>
                            <input type="text"name="fechaDeEntrada" disabled="true" value="<?php echo $rowFicha['fechaEntrada']; ?>" style="width:200px;">
                            <a>Ej: YYYY-MM-DD HH:MM:SS</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Cliente</a>
                        </td>
                        <td>
                            <select name="cliente" style="width: 200px" disabled="true" id="cliente" onChange='cargaContenido(this.id)' >
                                <?php while ($row = mysql_fetch_assoc($result_cliente)) { ?>
                                    <option value="<?php echo $row['id_cliente']; ?>" <?php
                                    if ($row['id_cliente'] == $rowFicha['id_cliente_ac']) {
                                        echo "selected=true";
                                    }
                                    ?>>
                                                <?php echo $row['nombre'] ?>
                                    </option>
                                <?php } ?>       
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Articulo</a>
                        </td>
                        <td>
                            <div id="articulos">
                                <select name='articuloCliente' id='articuloCliente' disabled="true">
                                    <option value='0'>Elige</option>
                                    <?php while ($row = mysql_fetch_assoc($result_articulo)) { ?>
                                        <option value="<?php echo $row['id_articulocliente']; ?>" <?php
                                        if ($row['id_articulocliente'] == $rowFicha['id_articulocliente']) {
                                            echo "selected=true";
                                        }
                                        ?>>
                                                    <?php echo $row['nombre'] . "-" . $row['marca'] . "-" . $row['modelo']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Problema</a>
                        </td>
                        <td>
                            <input type="text"name="problema" disabled="true" value="<?php echo $rowFicha['problema']; ?>" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Estado</a>
                        </td>
                        <td>
                            <input type="hidden" name="id" value="<?php echo $rowFicha['id_fichaTecnica']; ?>">
                            <select name="estado" disabled="true" style="width: 200px">
                                <?php while ($row = mysql_fetch_assoc($result_estado)) { ?>
                                    <option value="<?php echo $row['id_estado']; ?>" <?php
                                    if ($row['id_estado'] == $rowFicha['estado']) {
                                        echo "selected=true";
                                    }
                                    ?>>
                                                <?php echo $row['nombre'] ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Observaciones Ficha T&eacute;cnica</a>
                        </td>
                        <td>
                            <textarea rows="4" disabled="true" cols="50" style="width:400px; height:100px;"  name="observacionesFichaTecnica"><?php echo $rowFicha['observaciones_ft']; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                        <td>
                            <input type="button"  name="boton" value="Cancelar" class="btn" style="float: left" onclick=" location.href = 'javascript:history.back()'" >
                            <button type="submit" name="boton" value="guardar" class="btn btn-danger" style="float: right" >Confirmar</button>
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="fechaDeSalida" value="<?php echo $hoy; ?>" style="width:200px;">
            </form>
        </div>
    </body>
</html>
