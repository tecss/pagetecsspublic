<?php
include './Controllers/securityController.php';
include './Controllers/connection.php';
validarSesion();
establecerPermisoA();

$id = filter_input(INPUT_GET, 'id');
$query = "SELECT * FROM tecnico WHERE id_tecnico='" . $id . "'";
$result = mysql_query($query);
$row = mysql_fetch_assoc($result);
?>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>tecss-Eliminar Cliente</title>
    </head>
    <body>
        <?php include ("./nav.php"); ?>
        <div class="divContenedoraTabla tablaCarga">
            <div class="alert alert-danger">
            <h2>Eliminar Cliente</h2>
            </div>
            <form name="cargarTecnico" method="post" action="Controllers/eliminarTecnicoController.php?id=<?php echo $id ?>">
                <table>
                    <tr>
                        <td>
                            <a>Nombre</a>
                        </td>
                        <td>
                            <input type="text"name="nombre" disabled="true" style="width:200px;" value="<?php echo $row['nombre']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Tel&eacute;fono</a>
                        </td>
                        <td>
                            <input type="text" name="telefono" disabled="true" style="width:200px;" value="<?php echo $row['telefono']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input type="button"  name="boton" value="Cancelar" class="btn" style="float: left" onclick=" location.href = 'javascript:history.back()'" >
                            <button type="submit" name="boton" value="guardar" class="btn btn-danger" style="float: right" >Confirmar</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>