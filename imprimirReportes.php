<?php
static $INDEX_WEB = "../index.php";
include_once './Controllers/securityController.php';
include_once './Controllers/connection.php';
include_once './Controllers/FRASES.php';
include_once './Controllers/SecurityInjection.php';
validarSesion();
$hoy = date("Y-m-d");
$desde = filter_input(INPUT_GET, 'desde');
$desde = mysql_real_escape_string($desde);
$hasta = filter_input(INPUT_GET, 'hasta');
$hasta = mysql_real_escape_string($hasta);
if (empty($desde)) {
    $desde = "0000-00-00";
}
if (empty($hasta)) {
    $hasta = $hoy;
}
$id_cliente = filter_input(INPUT_GET, 'c');
$id_cliente = mysql_real_escape_string($id_cliente);
$estadoFichaTecnica = filter_input(INPUT_GET, 'ef');
$estadoFichaTecnica = mysql_real_escape_string($estadoFichaTecnica);
$desdec = date("Y-m-d", strtotime($desde));
$hastac = date("Y-m-d", strtotime($hasta));

if ($estadoFichaTecnica == "-1") {
    $query = "SELECT * FROM fichaTecnica AS ft "
            . "INNER JOIN (SELECT id_incidencia,fechaDeDestruccion,problema as problema_i,id_fichaTecnica,tarea_realizada,estado AS estado_i,fechaInicio,fechaFin "
            . "FROM incidencia) AS i ON ft.id_fichaTecnica=i.id_fichaTecnica "
            . "INNER JOIN (SELECT id_articulocliente,id_cliente AS id_cliente_ac, id_articulo AS id_articulo_ac FROM articuloCliente ) AS ac "
            . "ON ft.id_articulocliente=ac.id_articulocliente "
            . "INNER JOIN (SELECT id_cliente,nombre AS nombre_cliente FROM cliente WHERE id_cliente='$id_cliente' ) AS cliente "
            . "ON ac.id_cliente_ac=cliente.id_cliente "
            . "INNER JOIN (SELECT id_estadoFichaTecnica,nombre AS nombre_et FROM estadoFichaTecnica) AS et_ft "
            . "ON et_ft.id_estadoFichaTecnica=ft.estado "
            . "INNER JOIN (SELECT id_articulo,nombre AS nombre_articulo FROM articulo) AS articulo "
            . "ON ac.id_articulo_ac = articulo.id_articulo "
            . "WHERE fechaEntrada BETWEEN '$desdec' AND '$hastac 23:59:59.999' AND i.fechaDeDestruccion IS NULL "
            . "ORDER BY fechaEntrada DESC";
    if ($id_cliente == "-1") {
        $query = "SELECT * FROM fichaTecnica AS ft "
                . "INNER JOIN (SELECT id_incidencia,fechaDeDestruccion,problema as problema_i,id_fichaTecnica,tarea_realizada,estado AS estado_i,fechaInicio,fechaFin "
                . "FROM incidencia) AS i ON ft.id_fichaTecnica=i.id_fichaTecnica "
                . "INNER JOIN (SELECT id_articulocliente,id_cliente AS id_cliente_ac, id_articulo AS id_articulo_ac FROM articuloCliente ) AS ac "
                . "ON ft.id_articulocliente=ac.id_articulocliente "
                . "INNER JOIN (SELECT id_cliente,nombre AS nombre_cliente FROM cliente) AS cliente "
                . "ON ac.id_cliente_ac=cliente.id_cliente "
                . "INNER JOIN (SELECT id_estadoFichaTecnica,nombre AS nombre_et FROM estadoFichaTecnica) AS et_ft "
                . "ON et_ft.id_estadoFichaTecnica=ft.estado "
                . "INNER JOIN (SELECT id_articulo,nombre AS nombre_articulo FROM articulo) AS articulo "
                . "ON ac.id_articulo_ac = articulo.id_articulo "
                . "WHERE fechaEntrada BETWEEN '$desdec' AND '$hastac 23:59:59.999' AND i.fechaDeDestruccion IS NULL "
                . "ORDER BY fechaEntrada DESC";
    }
} else {
    $query = "SELECT * FROM fichaTecnica AS ft "
            . "INNER JOIN (SELECT id_incidencia,fechaDeDestruccion,problema as problema_i,id_fichaTecnica,tarea_realizada,estado AS estado_i,fechaInicio,fechaFin "
            . "FROM incidencia) AS i ON ft.id_fichaTecnica=i.id_fichaTecnica "
            . "INNER JOIN (SELECT id_articulocliente,id_cliente AS id_cliente_ac, id_articulo AS id_articulo_ac FROM articuloCliente ) AS ac "
            . "ON ft.id_articulocliente=ac.id_articulocliente "
            . "INNER JOIN (SELECT id_cliente,nombre AS nombre_cliente FROM cliente WHERE id_cliente='$id_cliente' ) AS cliente "
            . "ON ac.id_cliente_ac=cliente.id_cliente "
            . "INNER JOIN (SELECT id_estadoFichaTecnica,nombre AS nombre_et FROM estadoFichaTecnica) AS et_ft "
            . "ON et_ft.id_estadoFichaTecnica=ft.estado "
            . "INNER JOIN (SELECT id_articulo,nombre AS nombre_articulo FROM articulo) AS articulo "
            . "ON ac.id_articulo_ac = articulo.id_articulo "
            . "WHERE fechaEntrada BETWEEN '$desdec' AND '$hastac 23:59:59.999' AND id_estadoFichaTecnica='$estadoFichaTecnica' AND i.fechaDeDestruccion IS NULL "
            . "ORDER BY fechaEntrada DESC";
    if ($id_cliente == "-1") {
        $query = "SELECT * FROM fichaTecnica AS ft "
                . "INNER JOIN (SELECT id_incidencia,fechaDeDestruccion,problema as problema_i,id_fichaTecnica,tarea_realizada,estado AS estado_i,fechaInicio,fechaFin "
                . "FROM incidencia) AS i ON ft.id_fichaTecnica=i.id_fichaTecnica "
                . "INNER JOIN (SELECT id_articulocliente,id_cliente AS id_cliente_ac, id_articulo AS id_articulo_ac FROM articuloCliente ) AS ac "
                . "ON ft.id_articulocliente=ac.id_articulocliente "
                . "INNER JOIN (SELECT id_cliente,nombre AS nombre_cliente FROM cliente) AS cliente "
                . "ON ac.id_cliente_ac=cliente.id_cliente "
                . "INNER JOIN (SELECT id_estadoFichaTecnica,nombre AS nombre_et FROM estadoFichaTecnica) AS et_ft "
                . "ON et_ft.id_estadoFichaTecnica=ft.estado "
                . "INNER JOIN (SELECT id_articulo,nombre AS nombre_articulo FROM articulo) AS articulo "
                . "ON ac.id_articulo_ac = articulo.id_articulo "
                . "WHERE fechaEntrada BETWEEN '$desdec' AND '$hastac 23:59:59.999' AND id_estadoFichaTecnica='$estadoFichaTecnica' AND i.fechaDeDestruccion IS NULL "
                . "ORDER BY fechaEntrada DESC";
    }
}
$result = mysql_query($query) or die(mysql_error());

$query = "SELECT * FROM cliente WHERE id_cliente='$id_cliente'";
$result_c = mysql_query($query) or die(mysql_error());
$row_c = mysql_fetch_assoc($result_c);
?>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>Imprimir Resumen de Servicio </title>
        <style>
            #table-print{ 
                padding: 5px;
                font-size: 8px;
            }
        </style>
    </head>
    <body>
        <div class="divContenedoraTabla">
            <table>
                <tr>
                    <td rowspan="2" colspan="2">
                        <img src="images/can.png" style="" border="0" alt="canoffice" width="300px" height="">
                    </td>
                </tr>   
                <tr>
                    <td colspan="2">
                        <table  style="margin-left: 150px;" id="table-print">
                            
							<tr>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								
								<td>
									<center><h3> Resumen de Servicio T&eacutecnico</h3></center>
								</td>
							</tr>
							
							<tr>
                                <td>
								<center>
								<a style="color: #000000; font-size:14px"">Desde  </a>
								<?php echo $desde ?>
								<a style="color: #000000; font-size:14px">   Hasta </a>
								<?php echo $hasta ?>
								</center></td>
                            </tr>
							
							<tr>
								<td>
									<br>
									<br>
									<br>
									<br>
								</td>
							</tr>
							
							
							<!-- Este lo borre para probar
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
							-->
                        </table>
						
                    </td>
                </tr>
            </table>
            <table  id="table-print" >
			
                <thead>
                    <tr>
					
                        <td style="width: 3%;font-size: 12px;">Orden
                        </td>
						
						<td style="width: 15%;font-size: 12px;">Cliente
						</td>
                        
						<td style="width: 10%;font-size: 12px;">Art&iacuteculo
						</td>
                        
						<td style="width: 5%;font-size: 12px;">Estado  
                        </td>
						
					
                        <td style="width: 17%;font-size: 12px;">Diagn&oacute;stico    
                        </td>
						
                        <td style="width: 20%;font-size: 12px;">Tarea Realizada
                        </td>
						
						<td style="width: 20%;font-size: 12px;">Observaciones    
                        </td>
						
                        <td style="width: 5%;font-size: 12px;">Fecha    
                        </td>
						
                        <td style="width: 5%;font-size: 12px;">Duraci&oacute;n    
                        </td>
						
                    </tr>
                </thead>
				
                <tbody >
						<tr>
							<td colspan="9"> 
								<hr>
							</td>
						</tr>
                    <?php
                    //repetir "crear fila"
                    $duracionTotal = 0;
                    while ($row = mysql_fetch_assoc($result)) {
                        ?>
                        <tr id="cuerpoClienteTable">
                            <td style="width: 3%;">
                                <?php echo $row['id_negocio'] ?>
                            </td>
							
                            <td style="width: 15%;">
								<?php echo $row['nombre_cliente']; ?>
                            </td>
							
                            <td style="width: 10%;">
                                <?php echo $row['nombre_articulo']; ?>
                            </td>
							
							<td style="width:5%;">	
								<?php echo $row['nombre_et']; ?>
							  
                            </td>
							
                            <td style="width: 17%;">
                               <?php echo $row['problema_i']; ?>
                            </td>
							
                            <td style="width: 20%;">
                                <?php echo $row['tarea_realizada']; ?>
                            </td>
					
							<td style="width: 20%;">
                                <?php echo $row['observaciones']; ?>
                            </td>
							
                            <td style="width: 5%;">
                                <?php
                                $fechaInicio = date("Y-m-d", strtotime($row['fechaInicio']));
                                echo $fechaInicio;
                                ?>
                            </td>
                            <td style="width: 5%;">
                                <?php
                                $fechaInicio = strtotime($row['fechaInicio']);
                                $fechaFin = strtotime($row['fechaFin']);
                                $duracion = mktime(date('H', $fechaFin), date('i', $fechaFin), date('s', $fechaFin), date('n', $fechaFin), date('j', $fechaFin), date('Y', $fechaFin)) - mktime(date('H', $fechaInicio), date('i', $fechaInicio), date('s', $fechaInicio), date('n', $fechaInicio), date('j', $fechaInicio), date('Y', $fechaInicio));
                                $duracion = abs($duracion);
                                $horasdiferencia = floor(($duracion / 60) / 60); //Redondeamos con floor
                                $minutosdiferencia = ((($duracion / 60) / 60) - floor(($duracion / 60) / 60)) * 60;
                                $duracionStr = $horasdiferencia . " h  " . round($minutosdiferencia, 0) . " m";
                                echo $duracionStr;
                                ?>
                            </td>
                        </tr>
						<tr>
							<td colspan="9"> 
								<br/>
							</td>
						</tr>
                        <?php
                        $duracionTotal+=$duracion;
						
                    }
//hasta aca- repetir
                    ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?php
                            $horasdiferencia = floor(($duracionTotal / 60) / 60); //Redondeamos con floor
                            $minutosdiferencia = ((($duracionTotal / 60) / 60) - floor(($duracionTotal / 60) / 60)) * 60;
                            $duracionStr = $horasdiferencia . " h" . round($minutosdiferencia, 0) . " m";
                            echo $duracionStr;
                            ?>
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
