<?php

include_once './Controllers/securityController.php';
include_once './Controllers/connection.php';
include_once './Controllers/FRASES.php';

include_once './Controllers/SecurityInjection.php';
mysql_query("SET NAMES 'utf8'");
validarSesion();
$busqueda = filter_input(INPUT_POST, 'busqueda');
$busqueda = safe($busqueda);

if ($busqueda <> '') {
    $query = "SELECT * FROM fichaTecnica AS ft "
            . "INNER JOIN incidencia AS i ON ft.id_fichaTecnica=i.id_fichaTecnica "
            . "INNER JOIN (SELECT id_articulocliente,id_cliente AS id_cliente_ac, id_articulo AS id_articulo_ac FROM articuloCliente ) AS ac "
            . "ON ft.id_articulocliente=ac.id_articulocliente "
            . "INNER JOIN (SELECT id_cliente,nombre AS nombre_cliente FROM cliente ) AS cliente "
            . "ON ac.id_cliente_ac=cliente.id_cliente "
            . "INNER JOIN tecnico AS t "
            . "ON i.id_tecnico=t.id_tecnico "
            . "INNER JOIN (SELECT id_estadoFichaTecnica,nombre AS nombre_et FROM estadoFichaTecnica) AS et_ft "
            . "ON et_ft.id_estadoFichaTecnica=ft.estado "
            . "INNER JOIN (SELECT id_estado,nombre AS nombre_et_i FROM estadoIncidencia) AS et_i "
            . "ON et_i.id_estado=i.estado "
            . "INNER JOIN (SELECT id_articulo,nombre AS nombre_articulo,marca,modelo,especificaciones,observaciones FROM articulo) AS articulo "
            . "ON ac.id_articulo_ac = articulo.id_articulo "
            . "WHERE ft.fechaDeDestruccion IS NULL AND i.fechaDeDestruccion IS NULL AND (articulo.nombre_articulo LIKE '%$busqueda%' "
            . "OR articulo.marca LIKE '%$busqueda%' "
            . "OR articulo.modelo LIKE '%$busqueda%' "
            . "OR articulo.especificaciones LIKE '%$busqueda%' "
            . "OR articulo.observaciones LIKE '%$busqueda%' "
            . "OR nombre_et_i LIKE '%$busqueda%' "
            . "OR nombre_et LIKE '%$busqueda%' "
            . "OR t.nombre LIKE '%$busqueda%' "
            . "OR cliente.nombre_cliente LIKE '%$busqueda%') "
            . "ORDER BY fechaEntrada DESC LIMIT 50 ";
    $result = mysql_query($query);
} else {

    $query = "SELECT * FROM fichaTecnica AS ft "
            . "INNER JOIN (SELECT id_articulocliente,id_cliente AS id_cliente_ac, id_articulo AS id_articulo_ac FROM articuloCliente ) AS ac "
            . "ON ft.id_articulocliente=ac.id_articulocliente "
            . "INNER JOIN (SELECT id_cliente,nombre AS nombre_cliente FROM cliente ) AS cliente "
            . "ON ac.id_cliente_ac=cliente.id_cliente "
            . "INNER JOIN (SELECT id_estadoFichaTecnica,nombre AS nombre_et FROM estadoFichaTecnica) AS et_ft "
            . "ON et_ft.id_estadoFichaTecnica=ft.estado "
            . "INNER JOIN (SELECT id_articulo,nombre AS nombre_articulo FROM articulo) AS articulo "
            . "ON ac.id_articulo_ac = articulo.id_articulo WHERE fechaDeDestruccion IS NULL ORDER BY fechaEntrada DESC";
    $result = mysql_query($query);
}

?>

<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.min.css" rel="stylesheet" type="text/css"> 
        <LINK href="css/style.css" rel="stylesheet" type="text/css"> 
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>Men&uacute; Ficha T&eacute;cnica</title>
    </head>
    <body>
        <?php include ('./nav.php'); ?>
        <div class="divContenedoraTabla">
            <table class="table table-striped table-generic">
                <thead>
                    <tr>
                        <td>
                            ID
                        </td>
                        <td>
                            Fecha
                        </td>
                        <td>
                            Cliente
                        </td>
                        <td>
                            Art&iacuteculo
                        </td>
                        <td>
                            Problema
                        </td>
                        <td>
                            Estado
                        </td>
                        <td>

                        </td>
                    </tr>
                </thead>
                <tbody >
                    <?php
                    //repetir "crear fila"
                    while ($row = mysql_fetch_assoc($result)) {
                        ?>
                        <tr id="cuerpoClienteTable">
                            <td>
                                <?php echo $row['id_negocio'] ?>
                            </td>
                            <td >
                                <?php echo date("Y-m-d H:i", strtotime($row['fechaEntrada'])); ?>
                            </td>
                            <td >
                                <a href="clienteDetalle.php?id=<?php echo $row['id_cliente'] ?>"><?php echo $row['nombre_cliente']; ?></a>
                            </td>
                            <td >
                                <a href="articuloDetalle.php?id=<?php echo $row['id_articulo'] ?>"><?php echo $row['nombre_articulo']; ?></a>
                            </td>
                            <td >
                                <?php echo $row['problema']; ?>
                            </td>
                            <?php $estado = $row['nombre_et']; ?>

                            <td style="<?php
                            if (strtoupper($estado) != "CERRADO") {
                                if (strtoupper($estado) == "FACTURAR") {
                                    echo "color: red;";
                                } else {
                                    echo "color: green;";
                                }
                            }
                            ?>">
                                    <?php echo $estado; ?>
                            </td>
                            <td >
                                <div>
                                    <a href="fichaTecnicaDetalle.php?menu=fichaTecnicaDetalle&id=<?php echo $row['id_fichaTecnica']; ?>" class="btn btn-primary"><i class="icon-eye-open icon-white"></i><br/> <?php echo VER; ?></a>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
//hasta aca- repetir
                    ?>
                </tbody>
            </table>
        </div>
    </body>
</html>
