<?php
$hoy = date("Y-m-d H:i");
?>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.min.css" rel="stylesheet" type="text/css"> 
        <LINK href="css/style.css" rel="stylesheet" type="text/css"> 
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />

        <title>Asistencia Técnica CanOffice.es</title>
    </head>
    <body>

        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#" style="font-family: small-caps">Canoffice.es</a>
                </div>
            </div>
	    <div class="divContenedoraTabla tablaCarga">
		<h2>Asistencia T&eacute;cnica </h2>
                <h3>Enviar Incidencia</h3>
                <form name="solicitarFichaTecnica" method="post" action="Controllers/solicitarFichaTecnicaController.php" data-parsley-validate>
                    <table>
                        <tr>
                            <td>
                                <a>Fecha:</a>
                            </td>
                            <td>
                                <input readonly="1" type="text"name="fechaDeEntrada" value="<?php echo $hoy ?>"style="width:200px;" required>
                                <small>Ej: YYYY-MM-DD HH:MM:SS</small>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a>Cliente </a>
                            </td>
                            <td>
                                <input type="text"name="nombreCliente" required style="width:200px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a>Art&iacute;culo</a>
                            </td>
                            <td>
                                <input type="text"name="articulo" required="" style="width:200px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a>Problema</a>
                            </td>
                            <td>
                                <textarea cols="40" rows="5" required style="width:400px; height:100px;"  name="problema"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td>
                                <input type="button"  name="boton" value="Salir" class="btn" style="float: left" onclick=" location.href = 'javascript:history.back()'" />
                                <button type="submit" name="boton" value="guardar" class="btn btn-success" style="float: right" >Enviar</button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <script src="js/parsley.min.js"></script>
            <!-- fecha entrada
            problema
            articulo
            cliente
            
            -->
    </body>
</html>



