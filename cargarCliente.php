<?php
include './Controllers/securityController.php';
include './Controllers/connection.php';
include_once './Controllers/FRASES.php';
validarSesion();

$query = "SELECT * FROM abono ORDER BY id_abono ASC";
$result_abono = mysql_query($query);
?>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title><?php echo CARGAR_CLIENTE; ?></title>
    </head>
    <body>
<?php include ("nav.php"); ?>
        <div class="divContenedoraTabla tablaCarga">
            <h2><?php echo CARGAR_CLIENTE; ?></h2>
            <form name="cargarCliente" method="post" action="Controllers/cargarClienteController.php">
                <table>
                    <tr>
                        <td>
                            <a><?php echo ID_NEGOCIO; ?></a>
                        </td>
                        <td>
                            <input type="text"name="idNegocio" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo NOMBRE; ?></a>
                        </td>
                        <td>
                            <input type="text"name="nombre" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo EMAIL; ?></a>
                        </td>
                        <td>
                            <input type="text" name="email" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo DIRECCION; ?></a>
                        </td>
                        <td>
                            <input type="text"name="direccion" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo TELEFONO; ?></a>
                        </td>
                        <td>
                            <input type="text"name="telefono" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo FAX; ?></a>
                        </td>
                        <td>
                            <input type="text"name="fax" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo CONTACTO; ?></a>
                        </td>
                        <td>
                            <textarea rows="4" cols="50" style="width:400px; height:100px;"  name="contacto"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo ABONO; ?></a>
                        </td>
                        <td>
                            <select name="abono" style="width: 200px">
                                <?php while ($row = mysql_fetch_assoc($result_abono)) { ?>
                                    <option value="<?php echo $row['id_abono']; ?>" ><?php echo $row['nombre'] ?></option>
                                <?php } ?>       
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo OBSERVACIONES; ?></a>
                        </td>
                        <td>
                            <textarea rows="4" cols="50" style="width:400px; height:100px;"  name="observaciones"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                        <td>
                            <input type="button"  name="boton" value="<?php echo CANCELAR; ?>" class="btn" style="float: left" onclick=" location.href = 'javascript:history.back()'" >
                            <button type="submit" name="boton" value="guardar" class="btn btn-success" style="float: right" ><?php echo CONFIRMAR; ?></button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>