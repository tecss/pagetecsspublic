function nuevoAjax()
{
    /* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
     lo que se puede copiar tal como esta aqui */
    var xmlhttp = false;
    try
    {
        // Creacion del objeto AJAX para navegadores no IE
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e)
    {
        try
        {
            // Creacion del objet AJAX para IE
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (E)
        {
            if (!xmlhttp && typeof XMLHttpRequest != 'undefined')
                xmlhttp = new XMLHttpRequest();
        }
    }
    return xmlhttp;
}

// Declaro los selects que componen el documento HTML. Su atributo ID debe figurar aqui.
var listadoSelects = new Array();
listadoSelects[0] = "cliente";
listadoSelects[1] = "articuloCliente";

function buscarEnArray(array, dato)
{
    // Retorna el indice de la posicion donde se encuentra el elemento en el array o null si no se encuentra
    var x = 0;
    while (array[x])
    {
        if (array[x] == dato)
            return x;
        x++;
    }
    return null;
}

function cargaContenido()
{
    idSelectOrigen="cliente";
    // Obtengo la posicion que ocupa el select que debe ser cargado en el array declarado mas arriba
    var posicionSelectDestino = buscarEnArray(listadoSelects, idSelectOrigen) + 1;
    // Obtengo el select que el usuario modifico
    var selectOrigen = document.getElementById(idSelectOrigen);
    // Obtengo la opcion que el usuario selecciono
    var opcionSeleccionada = selectOrigen.options[selectOrigen.selectedIndex].value;
    // Si el usuario eligio la opcion "Elige", no voy al servidor y pongo los selects siguientes en estado "Selecciona opcion..."
    if (opcionSeleccionada == 0)
    {
        var x = posicionSelectDestino, selectActual = null;
        // Busco todos los selects siguientes al que inicio el evento onChange y les cambio el estado y deshabilito
        while (listadoSelects[x])
        {
            selectActual = document.getElementById(listadoSelects[x]);
            selectActual.length = 0;

            var nuevaOpcion = document.createElement("option");
            nuevaOpcion.value = 0;
            nuevaOpcion.innerHTML = "Selecciona Opci&oacute;n...";
            selectActual.appendChild(nuevaOpcion);
            selectActual.disabled = true;
            x++;
        }
    }
    // Compruebo que el select modificado no sea el ultimo de la cadena
    else if (idSelectOrigen != listadoSelects[listadoSelects.length - 1])
    {
        // Obtengo el elemento del select que debo cargar
        var idSelectDestino = listadoSelects[posicionSelectDestino];
        var selectDestino = document.getElementById(idSelectDestino);
        // Creo el nuevo objeto AJAX y envio al servidor el ID del select a cargar y la opcion seleccionada del select origen
        var ajax = nuevoAjax();
        ajax.open("GET", "./Controllers/http_request_getArticulosToCliente.php?selectCliente=" + opcionSeleccionada, true);
        ajax.onreadystatechange = function()
        {
            if (ajax.readyState == 1)
            {
                // Mientras carga elimino la opcion "Selecciona Opcion..." y pongo una que dice "Cargando..."
                selectDestino.length = 0;
                var nuevaOpcion = document.createElement("option");
                nuevaOpcion.value = 0;
                nuevaOpcion.innerHTML = '<img src="images/load16.gif" border="0" alt="editor" width="20" height="20">';
                selectDestino.appendChild(nuevaOpcion);
                selectDestino.disabled = true;
            }
            if (ajax.readyState == 4)
            {
                selectDestino.parentNode.innerHTML = ajax.responseText;
            }
        }
        ajax.send(null);
    }
}


function updateCustomer(obj) {
    destino = document.getElementById("clientes");
    document.getElementById("articuloCliente").disabled = "1";
    var ajax = nuevoAjax();
    ajax.open("GET", "./Controllers/http_request_getClientes.php", true);
    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 1)
        {
            // Mientras carga elimino la opcion "Selecciona Opcion..." y pongo una que dice "Cargando..."
        }
        if (ajax.readyState == 4)
        {
            destino.parentNode.innerHTML = ajax.responseText;
        }
    };
    ajax.send(null);
}

function validateArticulo(obj) {
    destino = document.getElementById("state");
    marca= document.getElementById("text.marca").value;
    modelo=document.getElementById("text.modelo").value;
    var ajax = nuevoAjax();
    ajax.open("GET", "./Controllers/http_request_validateArticulos.php?marca="+marca+"&modelo="+modelo, true);
    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 1)
        {
            // Mientras carga elimino la opcion "Selecciona Opcion..." y pongo una que dice "Cargando..."
        }
        if (ajax.readyState == 4)
        {
            destino.parentNode.innerHTML = ajax.responseText;
        }
    };
    ajax.send(null);
}
