<?php
include './Controllers/securityController.php';
validarSesion();
establecerPermisoA();
?>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>tecss-Cargar Cliente</title>
    </head>
    <body>
        <?php include("./nav.php") ?> 
        <div class="divContenedoraTabla tablaCarga">
            <h2>Cargar T&eacute;cnico</h2>
            <form name="cargarTecnico" method="post" action="Controllers/cargarTecnicoController.php">
                <table>
                    <tr>
                        <td>
                            <a>Nombre</a>
                        </td>
                        <td>
                            <input type="text"name="nombre" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Tel&eacute;fono</a>
                        </td>
                        <td>
                            <input type="text" name="telefono" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Email</a>
                        </td>
                        <td>
                            <input type="text" name="email" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Password</a>
                        </td>
                        <td>
                            <input type="text" name="password" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Permisos</a>
                        </td>
                        <td>
                            <select name="permiso" >
                                <option value="A" >A</option>
                                <option value="B" >B</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input type="button"  name="boton" value="Cancelar" class="btn"  style="float: left" onclick=" location.href = 'javascript:history.back()'" >
                            <button type="submit" name="boton" value="guardar" class="btn btn-success" style="float: right" >Confirmar</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
