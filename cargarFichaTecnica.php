<?php
include './Controllers/securityController.php';
include './Controllers/connection.php';
include_once './Controllers/FRASES.php';
validarSesion();
$usuario = $_SESSION['user'];
$query = "SELECT id_cliente,nombre FROM cliente ORDER BY nombre ASC";
$result_cliente = mysql_query($query);

$query = "SELECT id_tecnico,nombre FROM tecnico WHERE email='$usuario'";
$result_tecnico = mysql_query($query);

$query = "SELECT id_estado,nombre FROM estadoIncidencia ";
$result_estado = mysql_query($query);

$query = "SELECT id_estadoFichaTecnica AS id_estado,nombre FROM estadoFichaTecnica ";
$result_estadoFichaTecnica = mysql_query($query);

$hoy = date("Y-m-d H:i");
?>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <script type="text/javascript" src="js/system_ajax.js"></script>
        <title>Cargar Ficha T&eacute;cnica</title>
    </head>
    <body>
        <?php include("./nav.php") ?>
        <div class="divContenedoraTabla tablaCarga">
            <h2>Cargar Ficha T&eacute;cnica</h2>
            <form name="cargarFichaTecnica" method="post" action="Controllers/cargarFichaTecnicaController.php">
                <table>
                    <tr>
                        <td>
                            <h2 class="titulo">Ficha T&eacute;cnica</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Cliente</a>
                        </td>
                        <td>
                            <div id="clientes">
                                <select name="cliente" style="width: 200px" id="cliente" onChange='cargaContenido()' >
                                    <?php while ($row = mysql_fetch_assoc($result_cliente)) { ?>
                                        <option value="<?php echo $row['id_cliente'] ?>"><?php echo $row['nombre'] ?></option>
                                    <?php } ?>       
                                </select>
                                <a href="cargarCliente.php" target="_blank"><img src="images/add_us.png" border="0" alt="NewCustomer" width="20" height="20"></a>
                                <a href="javascript:updateCustomer(this.id);"><img src="images/updates.png" border="0" alt="Update" width="20" height="20"></a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Art&iacute;culo</a>
                        </td>
                        <td>
                            <div id="articulos">
                                <select disabled="disabled" name="articuloCliente" id="articuloCliente">
                                    <option value="0">Selecciona opci&oacute;n...</option>
                                </select>
                            </div>

                        </td>
                    </tr>
                                        <tr>
                        <td>
                            <a>Estado Ficha T&eacute;cnica</a>
                        </td>
                        <td>
                            <select name="estado" style="width: 200px">
                                <?php while ($row = mysql_fetch_assoc($result_estadoFichaTecnica)) { ?>
                                    <option value="<?php echo $row['id_estado']; ?>" selected="1"><?php echo $row['nombre'] ?></option>
                                <?php } ?>       
                            </select>

                        </td>
                    </tr>

                    <tr>
                        <td>
                            <a>Problema</a>
                        </td>
                        <td>
                            <input type="text"name="problema" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Observaciones Ficha T&eacute;cnica</a>
                        </td>
                        <td>
                            <textarea cols="40" rows="5" style="width:400px; height:100px;"  name="observacionesFichaTecnica"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2 class="titulo"><?php echo INCIDENCIA; ?></h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>T&eacute;cnico</a>
                        </td>
                        <td>
                            <select name="tecnico" style="width: 200px">
                                <?php while ($row = mysql_fetch_assoc($result_tecnico)) { ?>
                                    <option value="<?php echo $row['id_tecnico']; ?>" selected="1"><?php echo $row['nombre'] ?></option>
                                <?php } ?>       
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Diagn&oacute;stico</a>
                        </td>
                        <td>
                            <input type="text"name="diagnostico" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Tarea realizada</a>
                        </td>
                        <td>
                            <input type="text"name="tareaRealizada" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Estado incidencia</a>
                        </td>
                        <td>
                            <select name="estadoIncidencia" style="width: 200px">
				<?php while ($row = mysql_fetch_assoc($result_estado)) { ?>
                                    <option value="<?php echo $row['id_estado']; ?>" selected="1"><?php echo $row['nombre'] ?></option>
                                <?php } ?>       
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Observaciones Incidencia</a>
                        </td>
                        <td>
                            <textarea cols="40" rows="5" style="width:400px; height:100px;"  name="observacionesIncidencia"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Fecha de Entrada</a>
                        </td>
                        <td>
                            <input type="text"name="fechaDeEntrada" value="<?php echo $hoy ?>"style="width:200px;">
                            <small>Ej: YYYY-MM-DD HH:MM:SS</small>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Fecha de Salida </a>
                        </td>
                        <td>
                            <input type="text"name="fechaDeSalida" value="<?php echo $hoy ?>" style="width:200px;">
                            <small>Ej: YYYY-MM-DD HH:MM:SS</small>
                        </td>
                    </tr>

                    <tr>
                        <td>

                        </td>
                        <td>
                            <input type="button"  name="boton" value="Cancelar" class="btn" style="float: left" onclick=" location.href = 'javascript:history.back()'" >
                            <button type="submit" name="boton" value="guardar" class="btn btn-success" style="float: right" >Confirmar</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
