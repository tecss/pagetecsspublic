<?php
include './Controllers/securityController.php';
include './Controllers/connection.php';
validarSesion();
$permiso = $_SESSION['permiso'];

$id = filter_input(INPUT_GET, 'id');
$query = "SELECT * FROM tecnico WHERE id_tecnico='" . $id . "'";
$result = mysql_query($query);
$row = mysql_fetch_assoc($result);
?>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>tecss-Cargar Cliente</title>
    </head>
    <body>
        <?php include ("./nav.php"); ?>
        <div class="divContenedoraTabla">
            <h2>Editar T&eacute;cnico</h2>
            <form name="cargarTecnico" method="post" action="Controllers/editarTecnicoController.php?id=<?php echo $id ?>">
                <table>
                    <tr>
                        <td>
                            <a>Nombre</a>
                        </td>
                        <td>
                            <input type="text"name="nombre" style="width:200px;" value="<?php echo $row['nombre']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Telefono</a>
                        </td>
                        <td>
                            <input type="text" name="telefono" style="width:200px;" value="<?php echo $row['telefono']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Email</a>
                        </td>
                        <td>
                            <input type="text" name="email" style="width:200px;" value="<?php echo $row['email']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Password</a>
                        </td>
                        <td>
                            <input type="text" name="password" style="width:200px;" value="********">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Permisos</a>
                        </td>
                        <td>
                            <select name="permiso" >
                                <?php if($permiso=='A'){ ?><option value="A" <?php if ($row['permiso'] == "A") {
            echo "selected true";
        } ?> >A</option><?php } ?>
                                <option value="B" <?php if ($row['permiso'] == "B") {
            echo "selected true";
        } ?>>B</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input type="button"  name="boton" value="Cancelar" class="btn" style="float: left" onclick=" location.href = 'javascript:history.back()'" >
                            <button type="submit" name="boton" value="guardar" class="btn" style="float: right" >Confirmar</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>