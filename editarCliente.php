<?php
include './Controllers/securityController.php';
include './Controllers/connection.php';
validarSesion();
include_once './Controllers/FRASES.php';

$id = filter_input(INPUT_GET, 'id');
$query = "SELECT * FROM cliente INNER JOIN clienteAbono AS ca ON ca.id_cliente=cliente.id_cliente WHERE cliente.id_cliente='" . $id . "'";
$result = mysql_query($query);
$row = mysql_fetch_assoc($result);

$query = "SELECT * FROM abono ";
$result_abono = mysql_query($query);
?>
<html>
    <head>
        <title>tecss-Cargar Cliente</title>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <script type="text/javascript" src="js/system_ajax.js"></script>
    </head>
    <body>
        <?php include ("./nav.php"); ?>
        <div class="divContenedoraTabla tablaCarga">
            <h2>Editar Cliente</h2>
            <form name="cargarCliente" method="post" action="Controllers/editarClienteController.php?id=<?php echo $id ?>">
                <table>
                    <tr>
                        <td>
                            <a><?php echo ID_NEGOCIO; ?></a>
                        </td>
                        <td>
                            <input type="text"name="idNegocio" value="<?php echo $row['id_negocio']; ?>" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo NOMBRE; ?></a>
                        </td>
                        <td>
                            <input type="text"name="nombre" style="width:200px;" value="<?php echo $row['nombre']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo EMAIL; ?></a>
                        </td>
                        <td>
                            <input type="text" name="email" style="width:200px;" value="<?php echo $row['email']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo DIRECCION; ?></a>
                        </td>
                        <td>
                            <input type="text"name="direccion" style="width:200px;" value="<?php echo $row['direccion']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo TELEFONO; ?></a>
                        </td>
                        <td>
                            <input type="text"name="telefono" style="width:200px;" value="<?php echo $row['telefono']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo FAX; ?></a>
                        </td>
                        <td>
                            <input type="text"name="fax" style="width:200px;" value="<?php echo $row['fax']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo CONTACTO; ?></a>
                        </td>
                        <td>
                            <textarea rows="4" cols="50" style="width:400px; height:100px;"  name="contacto"><?php echo $row['contacto']; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a><?php echo ABONO; ?></a>
                        </td>
                        <td>
                            <input type="hidden" name="id_clienteAbono" value="<?php echo $row['id_clienteAbono']; ?>">
                            <select name="abono" style="width: 200px">
                                    <?php while ($row_abono = mysql_fetch_assoc($result_abono)) { ?>
                                        <option value="<?php echo $row_abono['id_abono']; ?>" <?php
                                        if ($row_abono['id_abono'] == $row['id_abono']) {
                                            echo "selected=true";
                                        }
                                        ?>>
                                                    <?php echo $row_abono['nombre'] ?>
                                        </option>
                                    <?php } ?>   
                                </select>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a><?php echo OBSERVACIONES; ?></a>
                            </td>
                            <td>
                                <textarea rows="4" cols="50" style="width:400px; height:100px;"  name="observaciones" ><?php echo $row['observaciones']; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td>
                                <input type="button"  name="boton" value="<?php echo CANCELAR; ?>" class="btn" style="float: left" onclick=" location.href = 'javascript:history.back()'" >
                                <button type="submit" name="boton" value="guardar" class="btn btn-warning" style="float: right" ><?php echo CONFIRMAR; ?></button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>