<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/estilos.css" rel="stylesheet" type="text/css"><link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <LINK href="css/bootstrap.min.css" rel="stylesheet" type="text/css"> 
        <title>tecss</title>
        <!-- jQuery library (served from Google) -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <!-- bxSlider Javascript file -->
        <script src="js/jquery.bxslider.min.js"></script>
        <!-- bxSlider CSS file -->
        <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="css/style-Login.css">
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <script type="text/javascript">
            $(document).ready(function() {
                $('.bxslider').bxSlider({
                    auto: true,
                    mode: 'fade',
                    captions: true
                });

            });
        </script>
    </head>

    <body id="body-principal">
        <img src="images/cabecera.png" style="margin-bottom: -20px;margin-left: 120px;" border="0" alt="head">
        <div id="espacioSeparador30h"><br/></div>
        <div style="width: 800px;height: 500px;margin-left: 15%;">
            <ul class="bxslider">
                <li><img src="images/pic1.jpg"/></li>
                <li><img src="images/pic2.jpg"/></li>
                <li><img src="images/pic3.jpg"/></li>
                <li><img src="images/pic4.jpg"/></li>
            </ul>
        </div>
        <div style="margin-left: 43%">
            <a href="solicitarFichaTecnica.php" class="btn btn-primary"><i class="icon-envelope icon-white"></i> Enviar Incidencia</a>       
            <br/>
        </div>
        <section class="container">
            <div class="login">
                <h1>Login TecssCanoffice.es</h1>
                <form name="Iniciar sesion" method="post" action="Controllers/validarUsuarioController.php" >
                    <p><input type="text" name="user" value="" placeholder="Username or Email"></p>
                    <p><input type="password" name="password" value="" placeholder="Password"></p>
                    <p class="remember_me">
                        <label>
                            <input type="checkbox" name="remember_me" id="remember_me">
                            Recordarme
                        </label>
                    </p>
                    <p class="submit"><input type="submit" name="commit" value="Login"></p>
                </form>
            </div>
        </section>
        <div id="espacioSeparador30h"><br/></div>
    </body>
</html>