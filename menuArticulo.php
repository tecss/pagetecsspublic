<?php
include_once './Controllers/securityController.php';
include_once './Controllers/connection.php';
include_once './Controllers/FRASES.php';
validarSesion();
$query = "SELECT * FROM articulo ORDER BY marca ASC";
$result = mysql_query($query);
?>

<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>Tecss - Men&uacute; Art&iacute;culo</title>
    </head>
    <body>
        <?php include ('./nav.php'); ?>
        <div class="divContenedoraTabla">
            <table class="table table-striped table-generic" >
                <tr>
                    <td>
                            Nombre   
                    </td>
                    <td>
                            Marca
                    </td>
                    <td>
                            Modelo   
                    </td>
                    <td>
                    </td>
                </tr>
                <?php
                //repetir "crear fila"
                $i = TRUE;
                while ($row = mysql_fetch_assoc($result)) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $row['nombre']; ?>
                        </td>
                        <td>
                            <?php echo $row['marca']; ?>
                        </td>
                        <td>
                            <?php echo $row['modelo']; ?>
                        </td>
                        <td>
                            <div style="padding-left:10%">
                                <a href="articuloDetalle.php?id=<?php echo $row['id_articulo']; ?>" class="btn btn-primary"><i class="icon-eye-open icon-white"></i><br/> <?php echo VER; ?></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                }
                //hasta aca- repetir
                ?>
            </table>
        </div>

    </body>
</html>