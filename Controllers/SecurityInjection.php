<?php
require_once 'class.inputfilter.php';
function safe($value) {
    $search = array("\\", "\x00", "\n", "\r", "'", '"', "\x1a");
    $replace = array("\\\\", "\\0", "\\n", "\\r", "\'", '\"', "\\Z");
    $value<-str_replace($search, $replace, $value);
    $ifilter = new InputFilter();
    $value = $ifilter->process($value);
    return $value ;
}
