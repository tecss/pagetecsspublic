<?php

include 'connection.php';
include './randomString.php';
static $GUARDAR_TEXT = "guardar";
static $INDEX_WEB = "../index.php";

$boton = filter_input(INPUT_POST, 'boton');
if ($boton == $GUARDAR_TEXT) {
    $tipo = filter_input(INPUT_POST, 'update');
    $tipo = mysql_real_escape_string($tipo);
    if ($tipo == "viejo") {
        $articulo = filter_input(INPUT_POST, 'articulo');
        $articulo = mysql_real_escape_string($articulo);
    } else {
        $id_art = RandomString();
        $nombre = filter_input(INPUT_POST, 'nombre');
        $nombre = mysql_real_escape_string($nombre);
        $marca = filter_input(INPUT_POST, 'marca');
        $marca = mysql_real_escape_string($marca);
        $modelo = filter_input(INPUT_POST, 'modelo');
        $modelo = mysql_real_escape_string($modelo);
        $linkFabricante = filter_input(INPUT_POST, 'linkFabricante');
        $linkFabricante = mysql_real_escape_string($linkFabricante);
        $linkPDF = filter_input(INPUT_POST, 'linkPDF');
        $linkPDF = mysql_real_escape_string($linkPDF);
        $linkDrivers = filter_input(INPUT_POST, 'linkDrivers');
        $linkDrivers = mysql_real_escape_string($linkDrivers);
        $especificaciones = filter_input(INPUT_POST, 'especificaciones');
        $especificaciones = mysql_real_escape_string($especificaciones);
        $observaciones = filter_input(INPUT_POST, 'observaciones_a');
        $observaciones = mysql_real_escape_string($observaciones);

        $sql = "INSERT articulo(id_articulo,nombre,marca,modelo,link_fabricante,link_pdf,link_drivers,especificaciones,observaciones) VALUES ("
                . "'$id_art '," . "'$nombre '," . "'$marca'," . "'$modelo'," . "'$linkFabricante'," . "'$linkPDF'," . "'$linkDrivers',"
                . "'$especificaciones'," . "'$observaciones')";
        echo $sql;
        $result = mysql_query($sql) or die(mysql_error());

        $articulo = $id_art;
    }
    $id_ac = RandomString();
    $cliente = filter_input(INPUT_POST, 'cliente');
    $cliente = mysql_real_escape_string($cliente);
    $nroSerie = filter_input(INPUT_POST, 'nroSerie');
    $nroSerie = mysql_real_escape_string($nroSerie);
    $observaciones = filter_input(INPUT_POST, 'observaciones_ac');
    $observaciones = mysql_real_escape_string($observaciones);
    $sql = "INSERT articuloCliente(id_articulocliente,id_cliente,id_articulo,nroSerie,observaciones) VALUES ("
            . "'$id_ac'," . "'$cliente'," . "'$articulo'," . "'$nroSerie'," . "'$observaciones')";
    echo $sql;
    mysql_query($sql) or die(mysql_error());
    echo '<meta charset="UTF-8" http-equiv="refresh" content="0;url=' . $INDEX_WEB . '" />';
} else {
    echo '<meta charset="UTF-8" http-equiv="refresh" content="0;url=' . $INDEX_WEB . '" />';
}
