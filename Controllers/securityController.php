<?php
function validarSesion() {
    session_start();
    if (!isset($_SESSION['user'])) {
        echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"0;URL=iniciarSesion.php\">";
        exit();
    }else{
        $usuario = $_SESSION['user'];
    }
}

function establecerPermisoA() {
    $permiso = $_SESSION['permiso'];
    if ($permiso != 'A') {
        echo '<meta charset="UTF-8" http-equiv="refresh" content="6;url=./index.php" />';
        echo '<img src="images/accesodenegado.png" border="0" alt="acceso denegado">';
        echo '<img style="float:right" src="images/denegado.jpg" border="0" alt="acceso denegado" width="200" height="200"">';
        echo '<h1>Se redireccionar&aacute; en breve...o haga click <a href="./index.php">aqu&iacute;</a></h1>';
        exit();
    } else {
        
    }
}
