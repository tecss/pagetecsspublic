<?php
include_once './Controllers/securityController.php';
include_once './Controllers/connection.php';
include_once './Controllers/FRASES.php';
validarSesion();
$query = "SELECT * FROM tecnico";
$result = mysql_query($query);

$usuario = $_SESSION['user'];

?>

<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>tecss - Men&uacute; T&eacute;cnicos</title>
    </head>
    <body>
        <?php include ("nav.php"); ?>
        <div class="divContenedoraTabla">
            <table class="table table-striped table-generic">
                <thead>
                    <tr>
                        <td>
                            Nombre   
                        </td>
                        <td>
                            Tel&eacute;fono
                        </td>
                        <td>
                            Email
                        </td>
                        <td>
                            Permiso
                        </td>
                        <td>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    //repetir "crear fila"
                    while ($row = mysql_fetch_assoc($result)) {
                        ?>
                        <tr>
                            <td>
                                <?php echo $row['nombre']; ?>
                            </td>
                            <td>
                                <?php echo $row['telefono']; ?>
                            </td>
                            <td>
                                <?php echo $row['email']; ?>
                            </td>
                            <td>
                                <?php echo $row['permiso']; ?>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <?php
                                    if(rtrim($usuario)==rtrim($row['email'])){ ?><a href="editarTecnico.php?id=<?php echo $row['id_tecnico']; ?>" class="btn btn-group"><i class="icon-pencil"></i> <?php echo EDITAR; ?></a>
                                    <a href="eliminarTecnico.php?id=<?php echo $row['id_tecnico']; ?>" class="btn btn-danger btn-group"><i class="icon-trash icon-white"></i> <?php echo ELIMINAR; ?></a><?php } ?>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                    //hasta aca- repetir
                    ?>
                </tbody>
            </table>
        </div>
    </body>
</html>