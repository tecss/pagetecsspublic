<?php
include './Controllers/securityController.php';
validarSesion();
include './Controllers/connection.php';
$query = "SELECT id_cliente,nombre FROM cliente ORDER BY nombre ASC";
$result_cliente = mysql_query($query);

$query = "SELECT * FROM articulo ORDER BY marca ASC";
$result_articulo = mysql_query($query);
?>

<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
	<link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <script type="text/javascript" src="js/system_ajax.js"></script>

        <title>tecss-Cargar Articulo</title>
        <script language='javascript'>
            function setReadOnly(obj) {
                if (obj.value == "nuevo")
                {
                    document.getElementById("table_nuevo").style.display = "block";
                    document.getElementById("table_viejo").style.display = "none";
                    document.getElementById("tipo").value = "nuevo";
                } else {
                    document.getElementById("table_nuevo").style.display = "none";
                    document.getElementById("table_viejo").style.display = "block";
                    document.getElementById("tipo").value = "viejo";
                }
            }

        </script>
    </head>
    <body>
        <?php include("./nav.php") ?> 
        <div class="divContenedoraTabla tablaCarga">
            <h2>Cargar Art&iacute;culo</h2>
            <form name="cargarArticulo" method="post" action="Controllers/cargarArticuloController.php">
                <table class="table_">
                    <tr>
                        <td colspan="1">
                            <input type=radio name="update" value="viejo" checked onclick="setReadOnly(this)">Art&iacute;culo Existente<br />
                        </td>
                        <td>
                            <input type=radio name="update" value="nuevo" onclick="setReadOnly(this)">Crear Art&iacute;culo
                        </td>
                    </tr>
                    <tr>
		<td colspan="3">
<br/>

                            <table id="table_viejo" class="table_">
                                <tr>
                                    <td>
                                        <a>Art&iacute;culo</a>
                                    </td>
                                    <td>
                                        <select name="articulo" style="width: 200px">
                                            <?php while ($row = mysql_fetch_assoc($result_articulo)) { ?>
                                                <option value="<?php echo $row['id_articulo'] ?>">
                                                    <?php echo $row['nombre'] . "-" . $row['marca'] . "-" . $row['modelo'] ?>
                                                </option>
                                            <?php } ?>       
                                        </select>
                                    </td>
                                    <td>
                                        <a href="cargarArticulo.php" target="_blank" ><img src="images/add_ar.png" border="0" alt="editor" width="20" height="20"></a>
                                    </td>
                                </tr>
                            </table>
                            <table id="table_nuevo" style="display: none" class="table_">
                                <tr>
                                    <td>       
                                        <a id="nombre">Nombre</a>
                                    </td>
                                    <td>
                                        <input id="text.nombre" type="text"name="nombre" style="width:200px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a>Marca</a>
                                    </td>
                                    <td>
                                        <input id="text.marca" type="text" name="marca" style="width:200px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a>Modelo</a>
                                    </td>
				    <td>
					<input id="text.modelo" type="text"name="modelo" style="width:200px;">
					<a href="javascript:validateArticulo('state');"><img src="images/updates.png" border="0" alt="Update" width="20" height="20"></a>
				   </td>
				<td>
		<div id="state">:|</div>
				</td>
                                </tr>
                                <tr>
                                    <td>
                                        <a>Link Fabricante</a>
                                    </td>
                                    <td>
                                        <input type="text"name="linkFabricante" style="width:200px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a>Link PDF</a>
                                    </td>
                                    <td>
                                        <input type="text"name="linkPDF" style="width:200px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a>Link Drivers</a>
                                    </td>
                                    <td>
                                        <input type="text"name="linkDrivers" style="width:200px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a>Especificaciones</a>
                                    </td>
                                    <td>
                                        <textarea rows="4" cols="50" style="width:400px; height:100px;"  name="especificaciones"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a>Observaciones</a>
                                    </td>
                                    <td>
                                        <textarea rows="4" cols="50" style="width:400px; height:100px;"  name="observaciones_a"></textarea>
                                    </td>
                                </tr>
                            </table>

			  <h4>Registrar Art&iacute;culo</h4>

                        </td>
                    </tr>
                    <tr>
			<td>
                            <a>Cliente</a>
                        </td>
                        <td>
                            <select name="cliente" style="width: 200px">
                                <?php while ($row = mysql_fetch_assoc($result_cliente)) { ?>
                                    <option value="<?php echo $row['id_cliente'] ?>"><?php echo $row['nombre'] ?></option>
                                <?php } ?>       
                            </select>
                            <a href="cargarCliente.php" target="_blank"><img src="images/add_us.png" border="0" alt="editor" width="20" height="20"></a>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <a >N&uacute;mero de Serie</a>
                        </td>
                        <td>
                            <input type="text"name="nroSerie" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Observaciones </a>
                        </td>
                        <td>
                            <textarea rows="4" cols="50" style="width:400px; height:100px;"  name="observaciones_ac"></textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <input type="button"  name="boton" value="Cancelar" class="btn" style="float: left" onclick=" location.href = 'javascript:history.back()'" >
                        </td>
                        <td>
                            <button type="submit" name="boton" value="guardar" class="btn btn-success" style="float: right" >Confirmar</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
