<?php
include './Controllers/securityController.php';
include './Controllers/connection.php';
validarSesion();
$id = filter_input(INPUT_GET, 'id');
$query = "SELECT * FROM articulo WHERE id_articulo='" . $id . "'";
$result = mysql_query($query);
$row = mysql_fetch_assoc($result);
?>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>tecss-Editar Art&iacute;culo</title>
    </head>
    <body>
        <?php include ("./nav.php"); ?>
        <div class="divContenedoraTabla tablaCarga">
            <h2>Editar Art&iacute;culo</h2>
            <form name="editarArticulo" method="post" action="Controllers/editarArticuloController.php?id=<?php echo $id ?>">
                <table>
                    <tr>
                        <td>
                            <a>Nombre</a>
                        </td>
                        <td>
                            <input type="text"name="nombre" value="<?php echo $row['nombre']; ?>" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Marca</a>
                        </td>
                        <td>
                            <input type="text" name="marca" value="<?php echo $row['marca']; ?>" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Modelo</a>
                        </td>
                        <td>
                            <input type="text"name="modelo" value="<?php echo $row['modelo']; ?>" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Link Fabricante</a>
                        </td>
                        <td>
                            <input type="text"name="linkFabricante" value="<?php echo $row['link_fabricante']; ?>" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Link PDF</a>
                        </td>
                        <td>
                            <input type="text"name="linkPDF" value="<?php echo $row['link_pdf']; ?>" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Link Drivers</a>
                        </td>
                        <td>
                            <input type="text"name="linkDrivers" value="<?php echo $row['link_drivers']; ?>" style="width:200px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Especificaciones</a>
                        </td>
                        <td>
                            <textarea rows="4" cols="50" style="width:400px; height:100px;"  name="especificaciones"><?php echo $row['especificaciones']; ?> </textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a>Observaciones</a>
                        </td>
                        <td>
                            <textarea rows="4" cols="50" style="width:400px; height:100px;"  name="observaciones"><?php echo $row['observaciones']; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                        <td>
                            <input type="button"  name="boton" value="Cancelar" class="btn" style="float: left" onclick=" location.href = 'javascript:history.back()'" >
                            <button type="submit" name="boton" value="guardar" class="btn btn-warning" style="float: right" >Confirmar</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>