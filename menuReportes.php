<?php
static $INDEX_WEB = "../index.php";
include_once './Controllers/securityController.php';
include_once './Controllers/connection.php';
include_once './Controllers/FRASES.php';
include_once './Controllers/SecurityInjection.php';
validarSesion();
$hoy = date("Y-m-d");
$desde = filter_input(INPUT_GET, 'desde');
$desde = mysql_real_escape_string($desde);
$hasta = filter_input(INPUT_GET, 'hasta');
$hasta = mysql_real_escape_string($hasta);
if (empty($desde)) {
    $desde = "0000-00-00";
}
if (empty($hasta)) {
    $hasta = $hoy;
}
$id_cliente = filter_input(INPUT_GET, 'c');
$id_cliente = mysql_real_escape_string($id_cliente);
$estadoFichaTecnica = filter_input(INPUT_GET, 'ef');
$estadoFichaTecnica = mysql_real_escape_string($estadoFichaTecnica);
$desdec = date("Y-m-d", strtotime($desde));
$hastac = date("Y-m-d", strtotime($hasta));

if ($estadoFichaTecnica == "-1") {
    $query = "SELECT * FROM fichaTecnica AS ft "
            . "INNER JOIN (SELECT id_incidencia,fechaDeDestruccion,problema as problema_i,id_fichaTecnica,tarea_realizada,estado AS estado_i,fechaInicio,fechaFin "
            . "FROM incidencia) AS i ON ft.id_fichaTecnica=i.id_fichaTecnica "
            . "INNER JOIN (SELECT id_articulocliente,id_cliente AS id_cliente_ac, id_articulo AS id_articulo_ac FROM articuloCliente ) AS ac "
            . "ON ft.id_articulocliente=ac.id_articulocliente "
            . "INNER JOIN (SELECT id_cliente,nombre AS nombre_cliente FROM cliente WHERE id_cliente='$id_cliente' ) AS cliente "
            . "ON ac.id_cliente_ac=cliente.id_cliente "
            . "INNER JOIN (SELECT id_estadoFichaTecnica,nombre AS nombre_et FROM estadoFichaTecnica) AS et_ft "
            . "ON et_ft.id_estadoFichaTecnica=ft.estado "
            . "INNER JOIN (SELECT id_articulo,nombre AS nombre_articulo FROM articulo) AS articulo "
            . "ON ac.id_articulo_ac = articulo.id_articulo "
            . "WHERE fechaEntrada BETWEEN '$desdec' AND '$hastac 23:59:59.999' AND i.fechaDeDestruccion IS NULL "
            . "ORDER BY fechaEntrada DESC";
    if ($id_cliente == "-1") {
        $query = "SELECT * FROM fichaTecnica AS ft "
                . "INNER JOIN (SELECT id_incidencia,fechaDeDestruccion,problema as problema_i,id_fichaTecnica,tarea_realizada,estado AS estado_i,fechaInicio,fechaFin "
                . "FROM incidencia) AS i ON ft.id_fichaTecnica=i.id_fichaTecnica "
                . "INNER JOIN (SELECT id_articulocliente,id_cliente AS id_cliente_ac, id_articulo AS id_articulo_ac FROM articuloCliente ) AS ac "
                . "ON ft.id_articulocliente=ac.id_articulocliente "
                . "INNER JOIN (SELECT id_cliente,nombre AS nombre_cliente FROM cliente) AS cliente "
                . "ON ac.id_cliente_ac=cliente.id_cliente "
                . "INNER JOIN (SELECT id_estadoFichaTecnica,nombre AS nombre_et FROM estadoFichaTecnica) AS et_ft "
                . "ON et_ft.id_estadoFichaTecnica=ft.estado "
                . "INNER JOIN (SELECT id_articulo,nombre AS nombre_articulo FROM articulo) AS articulo "
                . "ON ac.id_articulo_ac = articulo.id_articulo "
                . "WHERE fechaEntrada BETWEEN '$desdec' AND '$hastac 23:59:59.999' AND i.fechaDeDestruccion IS NULL "
                . "ORDER BY fechaEntrada DESC";
    }
} else {
    $query = "SELECT * FROM fichaTecnica AS ft "
            . "INNER JOIN (SELECT id_incidencia,fechaDeDestruccion,problema as problema_i,id_fichaTecnica,tarea_realizada,estado AS estado_i,fechaInicio,fechaFin "
            . "FROM incidencia) AS i ON ft.id_fichaTecnica=i.id_fichaTecnica "
            . "INNER JOIN (SELECT id_articulocliente,id_cliente AS id_cliente_ac, id_articulo AS id_articulo_ac FROM articuloCliente ) AS ac "
            . "ON ft.id_articulocliente=ac.id_articulocliente "
            . "INNER JOIN (SELECT id_cliente,nombre AS nombre_cliente FROM cliente WHERE id_cliente='$id_cliente' ) AS cliente "
            . "ON ac.id_cliente_ac=cliente.id_cliente "
            . "INNER JOIN (SELECT id_estadoFichaTecnica,nombre AS nombre_et FROM estadoFichaTecnica) AS et_ft "
            . "ON et_ft.id_estadoFichaTecnica=ft.estado "
            . "INNER JOIN (SELECT id_articulo,nombre AS nombre_articulo FROM articulo) AS articulo "
            . "ON ac.id_articulo_ac = articulo.id_articulo "
            . "WHERE fechaEntrada BETWEEN '$desdec' AND '$hastac 23:59:59.999' AND id_estadoFichaTecnica='$estadoFichaTecnica' AND i.fechaDeDestruccion IS NULL "
            . "ORDER BY fechaEntrada DESC";
    if ($id_cliente == "-1") {
        $query = "SELECT * FROM fichaTecnica AS ft "
                . "INNER JOIN (SELECT id_incidencia,fechaDeDestruccion,problema as problema_i,id_fichaTecnica,tarea_realizada,estado AS estado_i,fechaInicio,fechaFin "
                . "FROM incidencia) AS i ON ft.id_fichaTecnica=i.id_fichaTecnica "
                . "INNER JOIN (SELECT id_articulocliente,id_cliente AS id_cliente_ac, id_articulo AS id_articulo_ac FROM articuloCliente ) AS ac "
                . "ON ft.id_articulocliente=ac.id_articulocliente "
                . "INNER JOIN (SELECT id_cliente,nombre AS nombre_cliente FROM cliente) AS cliente "
                . "ON ac.id_cliente_ac=cliente.id_cliente "
                . "INNER JOIN (SELECT id_estadoFichaTecnica,nombre AS nombre_et FROM estadoFichaTecnica) AS et_ft "
                . "ON et_ft.id_estadoFichaTecnica=ft.estado "
                . "INNER JOIN (SELECT id_articulo,nombre AS nombre_articulo FROM articulo) AS articulo "
                . "ON ac.id_articulo_ac = articulo.id_articulo "
                . "WHERE fechaEntrada BETWEEN '$desdec' AND '$hastac 23:59:59.999' AND id_estadoFichaTecnica='$estadoFichaTecnica' AND i.fechaDeDestruccion IS NULL "
                . "ORDER BY fechaEntrada DESC";
    }
}
$result = mysql_query($query) or die(mysql_error());

$query = "SELECT id_cliente,nombre FROM cliente ORDER BY nombre ASC";
$result_cliente = mysql_query($query);

$query = "SELECT id_estadoFichaTecnica,nombre AS nombre_et FROM estadoFichaTecnica ORDER BY nombre ASC";
$result_estado = mysql_query($query);
?>
<html>
    <head>
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
        <LINK href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <LINK href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="/images/kcmpci.png" />
        <title>Tecss - Men&uacute; Reportes</title>
        <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <script>
            $(document).ready(function() {
                $("#datepickerDesde").datepicker();
                $("#datepickerHasta").datepicker();
            });
        </script>
    </head>
    <body>
        <?php include ('./nav.php'); ?>
        <div class="divContenedoraTabla">

            <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
            <h2>Generar Reportes</h2>
            <form name="f1" class="form-inline" method="GET" action="menuReportes.php">
                <input type="text" name="desde" class="input-medium" id="datepickerDesde" placeholder="Desde" value="<?php echo $desde; ?>">
                <br/>
                <br/>
                <input type="text" name="hasta" class="input-medium" id="datepickerHasta" placeholder="Hasta" value="<?php echo $hasta; ?>">
                <br/>
                <br/>
                <div id="clientes">
                    <select name="c" style="width: 200px" id="cliente" >
                        <option value="-1" selected="1">Todos</option>
                        <?php while ($row = mysql_fetch_assoc($result_cliente)) { ?>
                            <option value="<?php echo $row['id_cliente'] ?>" <?php
                            if ($row['id_cliente'] == $id_cliente) {
                                echo 'selected="1"';
                            }
                            ?>><?php echo $row['nombre'] ?></option>
                                <?php } ?>       
                    </select>
                </div>
                <br/>
                <div>
                    <select name="ef" style="width: 200px" id="estadoFichaTecnica" >
                        <option value="-1" selected="1">Todos</option>
                        <?php while ($row = mysql_fetch_assoc($result_estado)) { ?>
                            <option value="<?php echo $row['id_estadoFichaTecnica'] ?>" <?php
                            if ($row['id_estadoFichaTecnica'] == $estadoFichaTecnica) {
                                echo 'selected="1"';
                            }
                            ?>><?php echo $row['nombre_et'] ?></option>
                                <?php } ?>       
                    </select>
                </div>
                <br/>
                <div class="row">
                    <div class="span2">
                        <button type="submit" onclick="this.form.action = 'menuReportes.php';
                return true;"  style="float: left;" class="btn">Generar</button>
                        <button type="submit" onclick="f1.action = 'imprimirReportes.php';
                                return true;" style="float: right" class="btn btn-info"><i class="icon-print icon-white"></i> <?php echo IMPRIMIR; ?> </button>
                    </div>
                </div>


            </form>
        </div>
        <div class="divContenedoraTabla">
            <table class="table table-striped table-generic">
                <thead>
                    <tr>
                        <td>
                            ID
                        </td>
                        <td>
                            Cliente
                        </td>
                        <td>
                            Art&iacute;culo
                        </td>
                        <td>
                            Diagn&oacute;stico
                        </td>
                        <td>
                            Tarea Realizada
                        </td>
                        <td>
                            Estado
                        </td>
                        <td>
                            Observaciones
                        </td>
                        <td>
                            Fecha
                        </td>
                        <td>
                            Duraci&oacute;n
                        </td>
                        <td>

                        </td>
                    </tr>
                </thead>
                <tbody >
                    <?php
                    //repetir "crear fila"
                    $duracionTotal = 0;
                    while ($row = mysql_fetch_assoc($result)) {
                        ?>
                        <tr id="cuerpoClienteTable">
                            <td>
                                <?php echo $row['id_negocio'] ?>
                            </td>
                            <td >
                                <a href="clienteDetalle.php?id=<?php echo $row['id_cliente'] ?>"><?php echo $row['nombre_cliente']; ?></a>
                            </td>
                            <td >
                                <a href="articuloDetalle.php?id=<?php echo $row['id_articulo'] ?>"><?php echo $row['nombre_articulo']; ?></a>
                            </td>
                            <td >
                                <?php echo $row['problema_i']; ?>
                            </td>
                            <td >
                                <?php echo $row['tarea_realizada']; ?>
                            </td>
                            <?php $estado = $row['nombre_et']; ?>
                            <td style="<?php
                            if (strtoupper($estado) != "CERRADO") {
                                if (strtoupper($estado) == "FACTURAR") {
                                    echo "color: green;";
                                } else {
                                    echo "color: red;";
                                }
                            }
                            ?>">
                                    <?php echo $estado; ?>
                            </td>
                            <td >
                                <?php echo $row['observaciones']; ?>
                            </td>
                            <td>
                                <?php
                                $fechaInicio = date("Y-m-d", strtotime($row['fechaInicio']));
                                echo $fechaInicio;
                                ?>
                            </td>
                            <td>
                                <?php
                                $fechaInicio = strtotime($row['fechaInicio']);
                                $fechaFin = strtotime($row['fechaFin']);
                                $duracion = mktime(date('H', $fechaFin), date('i', $fechaFin), date('s', $fechaFin), date('n', $fechaFin), date('j', $fechaFin), date('Y', $fechaFin)) - mktime(date('H', $fechaInicio), date('i', $fechaInicio), date('s', $fechaInicio), date('n', $fechaInicio), date('j', $fechaInicio), date('Y', $fechaInicio));
                                $duracion = abs($duracion);
                                $horasdiferencia = floor(($duracion / 60) / 60); //Redondeamos con floor
                                $minutosdiferencia = ((($duracion / 60) / 60) - floor(($duracion / 60) / 60)) * 60;
                                $duracionStr = $horasdiferencia . " horas," . round($minutosdiferencia, 0) . " min";
                                echo $duracionStr;
                                ?>
                            </td>
                            <td >
                                <div>
                                    <a href="fichaTecnicaDetalle.php?menu=fichaTecnicaDetalle&id=<?php echo $row['id_fichaTecnica']; ?>" class="btn btn-primary"><i class="icon-eye-open icon-white"></i><br/> <?php echo VER; ?></a>
                                </div>
                            </td>

                        </tr>
                        <?php
                        $duracionTotal+=$duracion;
                    }
//hasta aca- repetir
                    ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?php
                            $horasdiferencia = floor(($duracionTotal / 60) / 60); //Redondeamos con floor
                            $minutosdiferencia = ((($duracionTotal / 60) / 60) - floor(($duracionTotal / 60) / 60)) * 60;
                            $duracionStr = $horasdiferencia . " horas," . round($minutosdiferencia, 0) . " min";
                            echo $duracionStr;
                            ?>
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
